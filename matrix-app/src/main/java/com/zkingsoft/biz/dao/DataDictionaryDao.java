package com.zkingsoft.biz.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.matrix.core.pojo.PaginationVO;
import com.zkingsoft.biz.bean.DataDictionary;

/**
 * @description (数据字典)
 * @author 姜友瑶
 * @date 2018-07-10 07:52
 */
public interface DataDictionaryDao{

	public int insert(@Param("item") DataDictionary dataDictionary);
   	
   	public int batchInsert(@Param("list") List<DataDictionary> dataDictionaryList);
   	
	public int updateByMap(Map<String, Object> modifyMap);
	
	public int updateByModel(@Param("record")DataDictionary dataDictionary);
	
	public int deleteByIds(@Param("list") List<String> list);
	
	public int deleteById(String id);

	public int deleteByModel(@Param("record") DataDictionary dataDictionary);
	
	public List<DataDictionary> selectInPage(@Param("record") DataDictionary dataDictionary, @Param("pageVo") PaginationVO pageVo);

	public List<DataDictionary> selectByModel(@Param("record") DataDictionary dataDictionary);
	
	public int selectTotalRecord(@Param("record") DataDictionary dataDictionary);
	
	public DataDictionary  selectById(String id);
	
	public DataDictionary  selectForUpdate(String id);
	
}