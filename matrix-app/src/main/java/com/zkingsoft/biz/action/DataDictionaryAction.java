package com.zkingsoft.biz.action;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.anotations.RemoveRequestToken;
import com.matrix.core.anotations.SaveRequestToken;
import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.constance.SystemMessageCode;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.ModelUtils;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.zkingsoft.biz.bean.DataDictionary;
import com.zkingsoft.biz.dao.DataDictionaryDao;
import com.zkingsoft.common.bean.SysUsers;

/**
 * @description 数据字典
 * @author 姜友瑶
 * @date 2018-07-10 07:52
 */
@Controller
@RequestMapping(value = "admin/dataDictionary")
public class DataDictionaryAction {

	@Autowired
	private DataDictionaryDao dataDictionaryDao;

	// 记录编辑前的值Before_Edit_Value
	public static final String BEV = "DataDictionary_BEV";

	/**
	 * 列表显示
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(DataDictionary dataDictionary, PaginationVO pageVo) {
		if (pageVo == null) {
			pageVo = new PaginationVO();
		}
		List<DataDictionary> dataList = dataDictionaryDao.selectInPage(dataDictionary, pageVo);
		AjaxResult result = new AjaxResult(AjaxResult.STATUS_SUCCESS, dataList,
				dataDictionaryDao.selectTotalRecord(dataDictionary));
		return result;
	}

	/**
	 * 新增
	 */
	@RemoveRequestToken
	@RequestMapping(value = "/addDataDictionary")
	public @ResponseBody AjaxResult addDataDictionary(DataDictionary dataDictionary) {
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		dataDictionary.setId(UUIDUtil.getRandomID());
		dataDictionary.setCreateBy(user.getSuName());
		dataDictionary.setUpdateBy(user.getSuName());
		int i = dataDictionaryDao.insert(dataDictionary);
		if (i > 0) {
			return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.ADD_SUCCES, "数据字典");
		} else {
			throw new GlobleException(SystemErrorCode.DATA_ADD_FAIL);
		}
	}

	/**
	 * 修改
	 */
	@RemoveRequestToken
	@RequestMapping(value = "/modifyDataDictionary")
	public @ResponseBody AjaxResult modifyDataDictionary(DataDictionary newDataDictionary) {
		DataDictionary oldDataDictionary = WebUtil.getSessionAttribute(BEV);
		int i = 0;
		Map<String, Object> modifyMap = null;
		try {
			if (!ModelUtils.isModified(oldDataDictionary, newDataDictionary)) {
				i = MatrixConstance.DML_SUCCESSS;
			}
			modifyMap = ModelUtils.comparePojo2Map(oldDataDictionary, newDataDictionary);
		} catch (Exception e) {
			throw new GlobleException(SystemErrorCode.DATA_UPDATE_FAIL, e, newDataDictionary);
		}
		if (modifyMap.size() > 0) {
			modifyMap.put("id", oldDataDictionary.getId());
			dataDictionaryDao.updateByMap(modifyMap);
		}
		i = MatrixConstance.DML_SUCCESSS;
		WebUtil.removeSessionAttribute(BEV);
		if (i > 0) {
			return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.UPDATE_SUCCES, "数据字典");
		} else {
			throw new GlobleException(SystemErrorCode.DATA_UPDATE_FAIL);
		}
	}

	/**
	 * 进入修改界面
	 */
	@SaveRequestToken
	@RequestMapping(value = "/editForm")
	public String editForm(String id) {
		DataDictionary dataDictionary;
		if (id != null) {
			dataDictionary = dataDictionaryDao.selectById(id);
			WebUtil.getRequest().setAttribute("obj", dataDictionary);
			WebUtil.setSessionAttribute(BEV, dataDictionary);
		}
		return "admin/sys/dataDictionary-form";
	}

	/**
	 * 删除
	 */
	@RequestMapping(value = "/del")
	public @ResponseBody AjaxResult del(String keys) {
		List<String> ids = StringUtils.strToCollToString(keys, ",");
		int i = dataDictionaryDao.deleteByIds(ids);
		if (i > 0) {
			return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.DELETE_SUCCES, i);
		} else {
			throw new GlobleException(SystemErrorCode.DATA_DELETE_FAIL);
		}
	}

}