package com.zkingsoft.common.bean;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

/**
 * 系统按钮
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月3日
 */
public class SysButton extends EntityDTO {

	@Extend
	private static final long serialVersionUID = 1L;

	private String btnId;

	private String btnKey;

	private String btnValue;

	public String getBtnId() {
		return btnId;
	}

	public void setBtnId(String btnId) {
		this.btnId = btnId;
	}

	public String getBtnKey() {
		return btnKey;
	}

	public void setBtnKey(String btnKey) {
		this.btnKey = btnKey;
	}

	public String getBtnValue() {
		return btnValue;
	}

	public void setBtnValue(String btnValue) {
		this.btnValue = btnValue;
	}

}