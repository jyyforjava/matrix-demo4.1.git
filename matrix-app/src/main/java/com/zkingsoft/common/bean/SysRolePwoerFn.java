package com.zkingsoft.common.bean;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

/**
 * 功能和按钮的关系
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月3日
 */
public class SysRolePwoerFn  extends EntityDTO{
	@Extend
	private static final long serialVersionUID = 1L; 

	
	private String  rpfId;
			
	
	/**
	 * 角色id
	 */
	private String  roleId;
			
	
	/**
	 * 功能id
	 */
	private String  fnId;
			
	
	/**
	 * 按钮s
	 */
	private String  rpfBtns;
			
	

	public String getRpfId() {
		return rpfId;
	}
   	
   	public void setRpfId(String rpfId) {
		this.rpfId=rpfId;
	}
   	

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getFnId() {
		return fnId;
	}

	public void setFnId(String fnId) {
		this.fnId = fnId;
	}

	public String getRpfBtns() {
		return rpfBtns;
	}
   	
   	public void setRpfBtns(String rpfBtns) {
		this.rpfBtns=rpfBtns;
	}
   	

 


  
}