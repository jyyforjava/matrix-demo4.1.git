package com.zkingsoft.common.bean;

import java.util.List;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

/**
 * 功能实体
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月3日
 */
public class SysFunction extends EntityDTO {

	@Extend
	private static final long serialVersionUID = 1L;

	private String fnId;

	private String fnIcon;

	private String fnPath;

	/**
	 * 是禁用吗
	 */
	private String fnIsDisable;

	/**
	 * 功能等级
	 */
	private Integer fnGrade;

	/**
	 * 父级功能
	 */
	private String fnParentId;

	private String fnName;

	/**
	 * 排序字段
	 */
	private Integer fnSequence;

	/**
	 * 功能code
	 */
	private String fnCode;

	/**
	 * 是在菜单栏显示吗
	 */
	private String fnShowMenu;
	

	/**
	 * 功能对应的按钮集合
	 */
	private List<SysFnBtnRel> sysFnBtnRel;
	
	
	private String fnBtns;
	
	/**
	 * 功能的子功能
	 */
	@Extend
	private List<SysFunction> childs;
	
	/**
	 * 在权限分配时用于标记是否拥有该项功能
	 */
	@Extend
	private boolean hasThisFn = false;

	/**
	 * 角色所拥有的功能按钮
	 */
	@Extend
	private String rpfBns;
	

	public String getFnBtns() {
		return fnBtns;
	}

	public void setFnBtns(String fnBtns) {
		this.fnBtns = fnBtns;
	}

	public String getRpfBns() {
		return rpfBns;
	}

	public void setRpfBns(String rpfBns) {
		this.rpfBns = rpfBns;
	}

	public List<SysFnBtnRel> getSysFnBtnRel() {
		return sysFnBtnRel;
	}

	public void setSysFnBtnRel(List<SysFnBtnRel> sysFnBtnRel) {
		this.sysFnBtnRel = sysFnBtnRel;
	}

	public boolean getHasThisFn() {
		return hasThisFn;
	}

	public void setHasThisFn(boolean hasThisFn) {
		this.hasThisFn = hasThisFn;
	}

	public List<SysFunction> getChilds() {
		return childs;
	}

	public void setChilds(List<SysFunction> childs) {
		this.childs = childs;
	}

	public String getFnShowMenu() {
		return fnShowMenu;
	}

	public void setFnShowMenu(String fnShowMenu) {
		this.fnShowMenu = fnShowMenu;
	}

	public String getFnId() {
		return fnId;
	}

	public void setFnId(String fnId) {
		this.fnId = fnId;
	}

	public String getFnIcon() {
		return fnIcon;
	}

	public void setFnIcon(String fnIcon) {
		this.fnIcon = fnIcon;
	}

	public String getFnPath() {
		return fnPath;
	}

	public void setFnPath(String fnPath) {
		this.fnPath = fnPath;
	}

	public String getFnIsDisable() {
		return fnIsDisable;
	}

	public void setFnIsDisable(String fnIsDisable) {
		this.fnIsDisable = fnIsDisable;
	}

	public Integer getFnGrade() {
		return fnGrade;
	}

	public void setFnGrade(Integer fnGrade) {
		this.fnGrade = fnGrade;
	}

	public String getFnParentId() {
		return fnParentId;
	}

	public void setFnParentId(String fnParentId) {
		this.fnParentId = fnParentId;
	}

	public String getFnName() {
		return fnName;
	}

	public void setFnName(String fnName) {
		this.fnName = fnName;
	}

	public Integer getFnSequence() {
		return fnSequence;
	}

	public void setFnSequence(Integer fnSequence) {
		this.fnSequence = fnSequence;
	}

	public String getFnCode() {
		return fnCode;
	}

	public void setFnCode(String fnCode) {
		this.fnCode = fnCode;
	}

}