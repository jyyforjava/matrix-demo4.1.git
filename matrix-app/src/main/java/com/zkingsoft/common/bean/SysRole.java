package com.zkingsoft.common.bean;

import java.util.List;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

/**
 * 角色类
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月3日
 */
public class SysRole extends EntityDTO{
	@Extend
	private static final long serialVersionUID = 1L; 

	
	private String  roleId;
			
	
	private String  roleName;
			
	
	private String  roleRemark;
			
	
	private String  companyId;
			
	
	/**
	 * 角色所拥有的平台
	 */
	private String  plates;
		
	/**
	 *功能集合 
	 */
	@Extend
	private List<SysFunction> fnList;
	
	
	

	public List<SysFunction> getFnList() {
		return fnList;
	}

	public void setFnList(List<SysFunction> fnList) {
		this.fnList = fnList;
	}

	public String getRoleId() {
		return roleId;
	}
   	
   	public void setRoleId(String roleId) {
		this.roleId=roleId;
	}
   	

	public String getRoleName() {
		return roleName;
	}
   	
   	public void setRoleName(String roleName) {
		this.roleName=roleName;
	}
   	

	public String getRoleRemark() {
		return roleRemark;
	}
   	
   	public void setRoleRemark(String roleRemark) {
		this.roleRemark=roleRemark;
	}
   	

	public String getCompanyId() {
		return companyId;
	}
   	
   	public void setCompanyId(String companyId) {
		this.companyId=companyId;
	}
   	

	public String getPlates() {
		return plates;
	}
   	
   	public void setPlates(String plates) {
		this.plates=plates;
	}
   	

	@Override
	public String toString() {
		return "{SysRole:{"
		+"roleId:"+roleId+","
		+"roleName:"+roleName+","
		+"roleRemark:"+roleRemark+","
		+"companyId:"+companyId+","
		+"plates:"+plates+","
		+"}}";
	}


  
}