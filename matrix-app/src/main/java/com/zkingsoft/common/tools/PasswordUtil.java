package com.zkingsoft.common.tools;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;

import com.matrix.core.tools.EncrypUtil;
import com.zkingsoft.common.bean.SysUsers;
/**
 * 密码工具类
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 11, 2017
 */
public class PasswordUtil {

	/**
	 * 密码加密 用户密码+注册时间
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月6日
	 * @param sysUsers
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public static String getEncrypUserPwd(SysUsers sysUsers) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		if (sysUsers.getSuRegisterTime() == null) {
			sysUsers.setSuRegisterTime(new Date());
		}
		String pwdSource = sysUsers.getSuPassword()
				+ DateFormatUtils.ISO_DATETIME_FORMAT.format(sysUsers.getSuRegisterTime());
		return EncrypUtil.getSha1(pwdSource);
	}

}
