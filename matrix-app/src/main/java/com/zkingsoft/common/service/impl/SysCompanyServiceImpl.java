package com.zkingsoft.common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.ModelUtils;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.zkingsoft.common.bean.SysCompany;
import com.zkingsoft.common.bean.SysRole;
import com.zkingsoft.common.bean.SysRolePwoerFn;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.dao.SysCompanyDao;
import com.zkingsoft.common.dao.SysRoleDao;
import com.zkingsoft.common.dao.SysRolePwoerFnDao;
import com.zkingsoft.common.service.SysCompanyService;

/**
 * 公司管理
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 11, 2017
 */
@Service
public class SysCompanyServiceImpl implements SysCompanyService {

	@Autowired
	private SysCompanyDao sysCompanyDao;
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private SysRolePwoerFnDao rolePwoerFnDao;

	@Override
	public int add(SysCompany sysCompany) {
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		sysCompany.setComId(UUIDUtil.getRandomID());
		sysCompany.setCreateBy(user.getSuName());
		sysCompany.setUpdateBy(user.getSuName());
		return sysCompanyDao.insert(sysCompany);

	}

	@Transactional
	@Override
	public int modifyByMap(SysCompany oldSysCompany, SysCompany newSysCompany) {
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		newSysCompany.setUpdateBy(user.getSuName());

		updatePower(newSysCompany);
		Map<String, Object> modifyMap = null;
		try {
			if (!ModelUtils.isModified(oldSysCompany, newSysCompany)) {
				return MatrixConstance.DML_SUCCESSS;
			}

			modifyMap = ModelUtils.comparePojo2Map(oldSysCompany, newSysCompany);
		} catch (Exception e) {
			throw new GlobleException(SystemErrorCode.DATA_UPDATE_FAIL, e, newSysCompany.getComName());
		}
		if (modifyMap.size() > 0) {
			modifyMap.put("comId", oldSysCompany.getComId());
			return sysCompanyDao.updateByMap(modifyMap);
		}
		return MatrixConstance.DML_SUCCESSS;
	}

	/**
	 * 更新公司员工权限
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月5日
	 * @param newSysCompany
	 */
	private void updatePower(SysCompany newSysCompany) {
		List<String> oldFunctions = null;
		String functions = sysCompanyDao.selectById(newSysCompany.getComId()).getComFunctions();
		if (functions == null || functions.equals("")) {
			oldFunctions = new ArrayList<>();
		} else {
			oldFunctions = StringUtils
					.strToCollToString(sysCompanyDao.selectById(newSysCompany.getComId()).getComFunctions(), ",");
		}
		List<String> newFunctions = new ArrayList<>();
		if (newSysCompany.getComFunctions() == null) {
			newFunctions = StringUtils.strToCollToString(newSysCompany.getComFunctions(), ",");
		}

		for (String old : oldFunctions) {
			// 如果新权限中不包含这个老的功能，则要更新改企业下所有的角色权限
			if (!newFunctions.contains(old)) {
				SysRole role = new SysRole();
				role.setCompanyId(newSysCompany.getComId());
				List<SysRole> roles = sysRoleDao.selectByModel(role);
				// 获取该公司下所有的权限信息
				List<SysRolePwoerFn> compalyRolePwoer = new ArrayList<>();
				for (SysRole tempRole : roles) {
					SysRolePwoerFn r = new SysRolePwoerFn();
					r.setRoleId(tempRole.getRoleId());
					compalyRolePwoer.addAll(rolePwoerFnDao.selectByModel(r));
				}
				// 记录要删除的id
				List<String> delRolePwoer = new ArrayList<>();
				for (SysRolePwoerFn rolePwoer : compalyRolePwoer) {
					// 如果有一个老权限则删除这个权限
					if (rolePwoer.getFnId() != null && rolePwoer.getFnId().equals(old)) {
						delRolePwoer.add(rolePwoer.getRpfId());
					}
				}
				// 如果获取到了就删除
				if (!delRolePwoer.isEmpty()) {
					rolePwoerFnDao.deleteByIds(delRolePwoer);
				}
			}
		}
	}

	@Override
	public int modifyByModel(SysCompany sysCompany) {

		return sysCompanyDao.updateByModel(sysCompany);

	}

	@Override
	public int remove(List<String> list) {

		return sysCompanyDao.deleteByIds(list);

	}

	@Override
	public int removeById(String comId) {

		return sysCompanyDao.deleteById(comId);

	}

	@Override
	public int removeByModel(SysCompany sysCompany) {

		return sysCompanyDao.deleteByModel(sysCompany);

	}

	@Override
	public List<SysCompany> findInPage(SysCompany sysCompany, PaginationVO pageVo) {

		return sysCompanyDao.selectInPage(sysCompany, pageVo);

	}

	@Override
	public List<SysCompany> findByModel(SysCompany sysCompany) {

		return sysCompanyDao.selectByModel(sysCompany);

	}

	@Override
	public int findTotal(SysCompany sysCompany) {

		return sysCompanyDao.selectTotalRecord(sysCompany);

	}

	@Override
	public SysCompany findById(String comId) {

		return sysCompanyDao.selectById(comId);

	}

}