package com.zkingsoft.common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.ModelUtils;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.zkingsoft.common.bean.SysFnBtnRel;
import com.zkingsoft.common.bean.SysFunction;
import com.zkingsoft.common.bean.SysRole;
import com.zkingsoft.common.bean.SysRolePwoerFn;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.dao.SysRoleDao;
import com.zkingsoft.common.dao.SysRolePwoerFnDao;
import com.zkingsoft.common.service.SysRoleService;

/**
 * 角色管理功能
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 11, 2017
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {

	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private SysRolePwoerFnDao rolePwoerDao;

	@Transactional
	@Override
	public int add(SysRole sysRole) {
		sysRole.setRoleId(UUIDUtil.getRandomID());
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		sysRole.setCompanyId(user.getCompanyId());
		sysRole.setCreateBy(user.getSuName());
		sysRole.setUpdateBy(user.getSuName());
		// 新增角色对应的中间表
		int i = sysRoleDao.insert(sysRole);
		addRolePwoer(sysRole);
		return i;

	}

	/**
	 * 新增角色的权限，添加前清空已有的权限
	 * 
	 * @param sysRole
	 */
	private void addRolePwoer(SysRole sysRole) {
		SysRolePwoerFn sysRolePwoerFn = new SysRolePwoerFn();
		sysRolePwoerFn.setRoleId(sysRole.getRoleId());
		rolePwoerDao.deleteByModel(sysRolePwoerFn);
		List<SysFunction> fnList = sysRole.getFnList();
		List<SysRolePwoerFn> rolePwoer = new ArrayList<>();
		if (fnList != null) {
			for (SysFunction sysFunction : fnList) {
				// 如果为空则不在进行添加
				if (sysFunction == null || sysFunction.getFnId() == null) {
					continue;
				}
				SysRolePwoerFn temp = new SysRolePwoerFn();
				temp.setRpfId(UUIDUtil.getRandomID());
				temp.setFnId(sysFunction.getFnId());
				temp.setRoleId(sysRole.getRoleId());
				temp.setCreateBy(sysRole.getCreateBy());
				temp.setUpdateBy(sysRole.getUpdateBy());
				temp.setRpfBtns(getBtns(sysFunction.getSysFnBtnRel()));
				rolePwoer.add(temp);
			}
		}
		if (CollectionUtils.isNotEmpty(rolePwoer)) {
			rolePwoerDao.batchInsert(rolePwoer);
		}
	}

	/**
	 * 获取功能按钮的字符串值
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月6日
	 * @param sysFunction
	 * @return
	 */
	private String getBtns(List<SysFnBtnRel> fnBtnRelList) {
		StringBuilder btns = new StringBuilder();
		if (CollectionUtils.isNotEmpty(fnBtnRelList)) {
			for (SysFnBtnRel fnBtnRel : fnBtnRelList) {
				if (fnBtnRel != null && StringUtils.isNotBlank(fnBtnRel.getBtnValue())) {
					btns.append(fnBtnRel.getBtnValue() + ",");
				}
			}
		}
		return btns.toString().substring(0, btns.toString().length());
	}

	@Override
	public int batchAdd(List<SysRole> sysRoleList) {
		return sysRoleDao.batchInsert(sysRoleList);
	}

	@Override
	public int modifyByMap(SysRole oldSysRole, SysRole newSysRole) {
		// 这样写只能保证基本信息不被覆盖
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		newSysRole.setCompanyId(user.getCompanyId());
		newSysRole.setCreateBy(user.getSuName());
		newSysRole.setUpdateBy(user.getSuName());
		addRolePwoer(newSysRole);
		Map<String, Object> modifyMap = null;
		try {
			if (!ModelUtils.isModified(oldSysRole, newSysRole)) {
				return MatrixConstance.DML_SUCCESSS;
			}
			modifyMap = ModelUtils.comparePojo2Map(oldSysRole, newSysRole);
		} catch (Exception e) {
			throw new GlobleException(SystemErrorCode.DATA_UPDATE_FAIL, e, newSysRole.getRoleName());
		}
		if (modifyMap.size() > 0) {
			modifyMap.put("roleId", oldSysRole.getRoleId());
			return sysRoleDao.updateByMap(modifyMap);
		}
		return MatrixConstance.DML_SUCCESSS;
	}

	@Override
	public int modifyByModel(SysRole sysRole) {
		return sysRoleDao.updateByModel(sysRole);
	}

	@Override
	public int remove(List<String> list) {

		return sysRoleDao.deleteByIds(list);

	}

	@Override
	public int removeById(String roleId) {

		return sysRoleDao.deleteById(roleId);

	}

	@Override
	public int removeByModel(SysRole sysRole) {

		return sysRoleDao.deleteByModel(sysRole);

	}

	@Override
	public List<SysRole> findInPage(SysRole sysRole, PaginationVO pageVo) {

		return sysRoleDao.selectInPage(sysRole, pageVo);

	}

	@Override
	public List<SysRole> findByModel(SysRole sysRole) {

		return sysRoleDao.selectByModel(sysRole);

	}

	@Override
	public int findTotal(SysRole sysRole) {

		return sysRoleDao.selectTotalRecord(sysRole);

	}

	@Override
	public SysRole findById(String roleId) {

		return sysRoleDao.selectById(roleId);

	}

}