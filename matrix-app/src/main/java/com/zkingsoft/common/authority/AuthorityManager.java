package com.zkingsoft.common.authority;
/**
 * Authority 类定义了Matrix 框架权限部分的主要接口
 * 这些接口被需要实现权限管理的类来实现。
 * @author jiangyouyao
 * @date 2016-12-10
 */
public interface AuthorityManager {
	/**
	 * 判断是否有权限访问按钮
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月9日
	 * @param matchStr
	 * @return
	 * @throws IllegalArgumentException
	 */
	public boolean isBtnPermitted(String matchStr) ;

	/**
	 * 判断是否有权限访问功能
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月9日
	 * @param fnCode
	 * @return
	 */
	public boolean isFnPermitted(String fnCode);

	/**
	 * 用户退出
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月9日
	 */
	public void getLoginOut();
	
	
}
