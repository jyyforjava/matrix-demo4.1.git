package com.zkingsoft.common.constance;

/**
 * 应用层的常量
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 10, 2017
 */
public class AppConstance {

	private AppConstance() {
	}

	/**
	 * 用户登录key
	 */
	public static final String DEFAULT_AUTHORITYMANAGER = "defaultAuthorityManager";

	// =====================================账号类型
	/**
	 * 平台管理员
	 */
	public static final String USER_TYPE_SUPER = "super";
	/**
	 * 开发人员
	 */
	public static final String USER_TYPE_DEVELOPER = "developer";
	/**
	 * 公司管理员
	 */
	public static final String USER_TYPE_ADMIN = "admin";
	/**
	 * 客户
	 */
	public static final String USER_TYPE_CUSTIMER = "customer";
	/**
	 * 公司员工
	 */
	public static final String USER_TYPE_EMPLOYEE = "employee";
	

	// =====================================账号状态
	/**
	 * 已激活
	 */
	public static final String ACCOUNT_STATUS_ACTIVATE = "activate";
	/**
	 * 未激活
	 */
	public static final String ACCOUNT_STATUS_INACTIVATED = "inactivated";
	/**
	 * 锁定
	 */
	public static final String ACCOUNT_STATUS_LOCKED = "locked";

	// =====================================是否类字段
	/**
	 * 是
	 */
	public static final String IS_Y = "是";
	public static final String IS_N = "否";

	// =====================================逻辑删除标识
	/**
	 * 未删除
	 */
	public static final String RECORD_VALID = "Y";

	/**
	 * 已删除
	 */
	public static final String RECORD_INVALID = "N";

	/** 一级功能 **/
	public static final Integer FUNCTION_FIRST_LEVEL = 1;

	/**
	 * 错误密码输入次数
	 */
	public static final String ERROR_PASSWORD_TIMES = "error_password_times";
	/**
	 * 不校验错误密码输入次数
	 */
	public static final String NOT_VALIDATE_ERROR_TIMES = "0";
	/**
	 * 默认密码
	 */
	public static final String DEFAULT_PASSWORD = "default_password";

	/**
	 * 登录失败标志
	 */
	public static final Integer LOGIN_FAIL = 2;
	/**
	 * 登录成功标志
	 */
	public static final Integer LOGIN_SUCCESS = 1;

	/**
	 * 权限拦截安全路径
	 */
	public static final String SAFEPATH = "/su";
	
	public static final String TOKEN_KEY = "token";
	/**
	 * 保存在cookie 中的token
	 */
	public static final String USER_TOKEN_COOKIE = "token";
	public static final String COOKIE_TIME_OUT = "cookie_time_out";
	/**
	 * nginx访问地址
	 */
	public static final String NGINX_URL = "static_resource_url";
	/**
	 * 存储路径
	 */
	public static final String FILES_TORAGE_PATH = "file_storage_path";

}
