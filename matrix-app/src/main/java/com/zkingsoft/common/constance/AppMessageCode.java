package com.zkingsoft.common.constance;

/**
 * 应用提示信息码
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月28日
 */
public interface AppMessageCode {

	interface Common{
		String OPERATION_SUCCESS="0000_0000";
	}
	
	
	/**
	 * 用户模块
	 */
	interface User {

		/** 密码修改成功！ **/
		String PWD_UPDATE_SUCCESS = "1001_0000";

		/** 密码修改失败！ **/
		String PWD_UPDATE_FAIL = "1001_0001";

		/** 旧密码输入不正确！ **/
		String PWD_OLD_INCORRECTLY = "1001_0002";
		
		/** 密码不能为空！ **/
		String PWD_IS_NULL = "1001_0003";
		
		/** 账号已经被占用 **/
		String ACCOUNT_EXIST = "1001_0004";
		
		/** 请输入账号和密码 **/
		String ACCOUNT_PASSWORD_MUST_REQUEST="1001_0005";
		
		/** 账号或密码错误 **/
		String ACCOUNT_PASSWORD_ERROR="1001_0006";
		
		/** 账号已被删除 **/
		String ACCOUNT_IS_DELETED="1001_0007";
		
		/** 账号未激活 **/
		String ACCOUNT_NOT_ACTIVE="1001_0008";
		
		/** 账号已经锁定 **/
		String ACCOUNT_IS_LOCK="1001_0009";

		/**账号不存在 **/
		String ACCOUNT_NOT_EXIST = "1001_0010";
		/** 获取用户信息失败，请重新登录 **/
		String LOGIN_TIMEOUT = "1001_0011";
	}

	interface Function {
		/** 功能的父级不能是自己 **/
		String PARENT_FUNCTION_CANNOT_BE_SELF = "1002_0000";

	}

}
