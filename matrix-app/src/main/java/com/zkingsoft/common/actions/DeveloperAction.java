package com.zkingsoft.common.actions;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.web.BaseAction;

/**
 * @description 管理员总action
 * @author 姜友瑶
 * @email 935090232@qq.com
 * @date 2016-06-26
 */
@Controller
@RequestMapping(value = "developer")
public class DeveloperAction extends BaseAction {

	/**
	 * 
	 * @Description: 页面定向方法，每个权限模块公用一个，每个模块共享一个一级路径，已便于进行权限过滤
	 * @author:姜友瑶
	 * @param page1
	 * @param page2
	 * @return 返回类型 String
	 * @date 2016年8月31日
	 */
	@RequestMapping(value = "/redirect/{page1}/{page2}")
	public String redirect(@PathVariable("page1") String page1, @PathVariable("page2") String page2) {
		return "developer/" + page1 + "/" + page2;
	}

	/**
	 * 
	 * @Description: 页面定向方法，每个权限模块公用一个，每个模块共享一个一级路径，已便于进行权限过滤
	 * @author:姜友瑶
	 * @param page1
	 * @param page2
	 * @return 返回类型 String
	 * @date 2016年8月31日
	 */
	@RequestMapping(value = "/redirect/{page1}")
	public String redirect(@PathVariable("page1") String page1) {
		return "developer/" + page1;
	}

	/**
	 * 关闭debug模式
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @return
	 */
	@RequestMapping("/closeDebug")
	public @ResponseBody AjaxResult closeDebug() {
		MatrixConstance.setDebugflag(false);
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, "debug模式关闭");
	}

	/**
	 * 打开debug模式
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @return
	 */
	@RequestMapping("/openDebug")
	public @ResponseBody AjaxResult openDebug() {
		MatrixConstance.setDebugflag(true);
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, "debug模式开启");
	}

}