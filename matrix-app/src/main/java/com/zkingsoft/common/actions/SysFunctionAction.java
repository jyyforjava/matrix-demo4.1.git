package com.zkingsoft.common.actions;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.SysFunction;
import com.zkingsoft.common.constance.AppConstance;
import com.zkingsoft.common.constance.AppVocabularyCode;
import com.zkingsoft.common.service.SysButtonService;
import com.zkingsoft.common.service.SysFunctionService;

/**
 * @Description: 系统功能管理
 * @author:姜友瑶
 * @date 2016年11月16日
 */
@Controller
@RequestMapping(value = "admin/sysFunction")
public class SysFunctionAction extends BaseAction {

	@Autowired
	private SysFunctionService sysFunctionService;

	@Autowired
	private SysButtonService sysBtnService;

	public static final String BEV = "SYSFUNCTION_BEV";

	/**
	 * 列表显示
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2016年12月10日
	 * @param sysFunction
	 * @param pageVo
	 * @return
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(SysFunction sysFunction, PaginationVO pageVo) {
		return showList(sysFunctionService, sysFunction, pageVo);
	}

	/**
	 * 显示所有功能
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @return
	 */
	@RequestMapping(value = "/all")
	public @ResponseBody AjaxResult all() {
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, sysFunctionService.findByModel(null));
	}

	/**
	 * 
	 * @Description: 修改查询function，在session保存旧值
	 * @author:姜友瑶
	 * @param sysFunction
	 * @param pageVo
	 * @return 返回类型 AjaxResult
	 * @date 2016年11月16日
	 */
	@RequestMapping(value = "/findById")
	public @ResponseBody AjaxResult findById(SysFunction sysFunction) {
		sysFunction = sysFunctionService.findById(sysFunction.getFnId());
		WebUtil.setSessionAttribute(BEV, sysFunction);
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, Arrays.asList(sysFunction));
	}

	/**
	 * 新增或者修改页面
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysFunction
	 * @return
	 */
	@RequestMapping(value = "/addFunction")
	public @ResponseBody AjaxResult addFunction(SysFunction sysFunction) {
		return add(sysFunctionService, sysFunction, AppVocabularyCode.FUNCTION);
	}

	/**
	 * 更新功能
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysFunction
	 * @return
	 */
	@PostMapping(value = "/modifyFunction")
	public @ResponseBody AjaxResult modifyFunction(SysFunction sysFunction) {
		AjaxResult result = modify(sysFunctionService, WebUtil.getSessionAttribute(BEV), sysFunction,
				AppVocabularyCode.FUNCTION);
		// 因为页面是一直打开的需要存新的值
		WebUtil.setSessionAttribute(BEV, sysFunction);
		return result;
	}

	/**
	 * 启用功能
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param fnId
	 * @return
	 */
	@RequestMapping(value = "/enableFunction")
	public @ResponseBody AjaxResult enableFunction(String fnId) {
		sysFunctionService.setIsDisable(fnId, AppConstance.IS_N);
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, "功能启用成功");
	}

	/**
	 * 禁用功能
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param fnId
	 * @return
	 */
	@RequestMapping(value = "/disEnableFunction")
	public @ResponseBody AjaxResult disEnableFunction(String fnId) {
		sysFunctionService.setIsDisable(fnId, AppConstance.IS_Y);
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, "功能禁用成功");
	}

	/**
	 * 进入修改界面
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/editForm")
	public String editForm(String id) {
		SysFunction sysFunction;
		if (id != null) {
			sysFunction = sysFunctionService.findById(id);
			// 查询出所有的按钮
			WebUtil.setRequestAttribute("obj", sysFunction);
			WebUtil.setSessionAttribute(BEV, sysFunction);
		}
		WebUtil.setRequestAttribute("btnList", sysBtnService.findByModel(null));
		return "developer/sysFunction-form";
	}

	/**
	 * 删除功能
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param fnId
	 * @return
	 */
	@RequestMapping(value = "/del")
	public @ResponseBody AjaxResult del(String fnId) {
		return remove(sysFunctionService, fnId);
	}

}