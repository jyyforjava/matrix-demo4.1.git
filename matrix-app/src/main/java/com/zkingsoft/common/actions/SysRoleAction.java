package com.zkingsoft.common.actions;

import static com.zkingsoft.common.constance.AppConstance.SAFEPATH;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.anotations.RemoveRequestToken;
import com.matrix.core.anotations.SaveRequestToken;
import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.SysFunction;
import com.zkingsoft.common.bean.SysRole;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.constance.AppVocabularyCode;
import com.zkingsoft.common.service.SysFunctionService;
import com.zkingsoft.common.service.SysRoleService;
/**
 * 功能管理
 * 
 * @author jiangyouyaoaa
 *
 */
@Controller
@RequestMapping(value = "admin/sysRole")
public class SysRoleAction extends BaseAction {

	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private SysFunctionService sysFunctionService;


	public static final String BEV = "SYSROLE_BEV";

	/**
	 * 列表显示
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysRole
	 * @param pageVo
	 * @return
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(SysRole sysRole, PaginationVO pageVo) {
		return showList(sysRoleService, sysRole, pageVo);
	}

	/**
	 * 列表显示当前登录人公司的列表
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @return
	 */
	@RequestMapping(value =SAFEPATH+"/showCompanyRole")
	public @ResponseBody AjaxResult showCompanyRole(SysRole sysRole, PaginationVO pageVo) {
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		sysRole.setCompanyId(user.getCompanyId());
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, sysRoleService.findInPage(sysRole, pageVo),sysRoleService.findTotal(sysRole));
	}

	/**
	 * 新增权限
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysRole
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value =SAFEPATH+"/addRole")
	public @ResponseBody AjaxResult addRole(SysRole sysRole) {
		return add(sysRoleService, sysRole, AppVocabularyCode.ROLE);
	}

	/**
	 * 修改权限
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysRole
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value =SAFEPATH+"/modifyRole")
	public @ResponseBody AjaxResult modifyRole(SysRole sysRole) {
		AjaxResult result = modify(sysRoleService, WebUtil.getSessionAttribute(BEV), sysRole, AppVocabularyCode.ROLE);
		WebUtil.removeSessionAttribute(BEV);
		return result;
	}

	/**
	 * 进入修改界面
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param id
	 * @return
	 */
	@SaveRequestToken
	@GetMapping(value =SAFEPATH+"/editForm")
	public String editForm(String id) {
		SysRole sysRole;
		List<SysFunction> functions =null;
		if (id != null) {
			sysRole = sysRoleService.findById(id);
			// 获取所有的功能
			functions = sysFunctionService.findRoleFuntion(id);
			WebUtil.getRequest().setAttribute("obj", sysRole);
			WebUtil.setSessionAttribute(BEV, sysRole);
		} else {
			functions = sysFunctionService.findRoleFuntion(null);
		}
		LogUtil.debug("查询出来的权限信息={}", functions);
		WebUtil.setRequestAttribute("functions", functions);
		return "admin/sys/sysRole-form";
	}

	/**
	 * 删除
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param keys
	 * @return
	 */
	@RequestMapping(value =SAFEPATH+"/del")
	public @ResponseBody AjaxResult del(String keys) {
		return remove(sysRoleService, keys);
	}

}