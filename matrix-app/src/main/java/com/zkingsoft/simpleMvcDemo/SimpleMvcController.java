package com.zkingsoft.simpleMvcDemo;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PageData;
import com.matrix.core.simpleDao.DaoSupport;
import com.matrix.core.tools.WebUtil;

/**
 * simple mvc demo
 * 
 * @author jiangyouyao
 *
 */
@Controller
@RequestMapping(value = "/simplemvc")
public class SimpleMvcController {

	@Resource(name = "daoSupport")
	private DaoSupport dao;

	@RequestMapping(value = "/add")
	private @ResponseBody AjaxResult addUser(){
		int i=1/0;
		PageData pd = WebUtil.getPageData();
		dao.save("simpleMvcDao.add", pd);
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, "用户添加成功");
	} 

	@RequestMapping(value = "/showList")
	private @ResponseBody AjaxResult showList(){
		PageData pd = WebUtil.getPageData();
		return new AjaxResult(AjaxResult.STATUS_SUCCESS,
				(List<Object>) dao.findForList("simpleMvcDao.selectInPage", pd),
				(Integer) dao.findForObject("simpleMvcDao.selectTotal", pd));
	}

}
