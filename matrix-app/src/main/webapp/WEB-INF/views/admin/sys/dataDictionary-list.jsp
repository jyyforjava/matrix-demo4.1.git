<%--   jsp的基本标签引入，和访问路径变量path，以及自定义标签matrix的引入 --%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<%--   End --%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
<%--   End --%>

</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 搜索框部分start -->
		<matrix:btn value="dictionary:search">
			<div class="row">
				<div class="col-sm-12">
					<form class="form-inline" id="serchform">
						<div class="input-group">
							<div class="form-group mr-20">
								<input name="value" placeholder="字典值" type="text"
									class="form-control">
							</div>
							<div class="form-group mr-20">
								<input name="typeName" placeholder="数据类型名称" type="text"
									class="form-control">
							</div>
							<div class="form-group mr-20">
								<button onclick="myGrid.serchData(1)" type="button"
									class="btn btn-info">
									<i class="fa fa-search "></i> 搜索
								</button>
								<button type="reset" class="btn btn-info ">
									<i class="fa fa-refresh "></i> 重置
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</matrix:btn>
		<!-- 搜索框部分en -->
		<div class="ibox-content radius-5 mt-5 mpanel">
			<div class="row">
				<div class="col-sm-12">
					<!-- 功能按钮部分 -->
					<div class="option-bar">
						<matrix:btn value="dictionary:dels">
							<button onclick="myGrid.delItems('id')" type="button"
								class="btn btn-danger btn-sm">
								<i class="fa fa-trash"></i>批量删除
							</button>
						</matrix:btn>
						<matrix:btn value="dictionary:add">
							<button onclick="openAdd()" type="button"
								class="btn btn-success btn-sm">
								<i class="fa fa-plus"></i> 新增
							</button>
						</matrix:btn>
					</div>
					<!-- 功能按钮部分end -->

					<!-- 数据表格部分 -->
					<table id="mgrid">
						<thead>
							<tr>
								<th data-checkbox="true"></th>
								<th data-formatter="MGrid.indexfn" data-align="center"
									data-width="30px">序号</th>
								<th data-field="id">主键</th>
								<th data-field="parentId">上级主键</th>
								<th data-field=typeName data-sortable="true">类型名称</th>
								<th data-field="typeCode">类型码</th>
								<th data-field="value">字典值</th>
								<th data-field="sort">排序</th>
								<th data-align="center" data-width="150px" data-field="id"
									data-formatter="buidOperate">操作</th>
							</tr>
						</thead>
					</table>
					<!-- 数据表格部分end -->
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
		//定义表格对象
		var myGrid;

		$(function() {

			//定义删除数据的路径
			var delPath = "";
			<matrix:btn value="dictionary:del">
			delPath = "${path}/do/admin/dataDictionary/del";
			</matrix:btn>
			//初始化表格对象
			myGrid = MGrid.initGrid({
				url : "${path}/do/admin/dataDictionary/showList",//url数据加载地址，表格初始化后会自动去后台加载数据。
				delUrl : delPath
			});

		});
		
		//构建操作栏的按钮
		function buidOperate(value, row, index) {
			var html = [];
			<matrix:btn value="dictionary:edit">
			html[0] = '<a onClick="openEdit(\'' + value
					+ '\')"  title="编辑" class="fa fa-edit option"></a>'
			</matrix:btn>

			<matrix:btn value="dictionary:del">
			html[3] = '<a onClick="myGrid.delItem(\''
					+ value
					+ '\')" title="删除" class="fa fa-close option text-danger"></a>';
			</matrix:btn>

			return html.join("");
		}
		<matrix:btn value="dictionary:add">
		//打开添加界面
		function openAdd() {
			layer.open({
				type : 2,
				title : "添加字典",
				maxmin : true,
				area : [ MUI.SIZE_L, '500px' ],
				content : [ '${path}/do/admin/dataDictionary/editForm' ]
			});
		}
		</matrix:btn>
		<matrix:btn value="dictionary:edit">
		//打开编辑界面
		function openEdit(id) {
			layer.open({
				type : 2,
				title : "编辑字典",
				area : [ MUI.SIZE_L, '500px' ],
				maxmin : true,
				content : [ '${path}/do/admin/dataDictionary/editForm?id=' + id ]
			});
		}
		</matrix:btn>

  
	</script>
</body>
</html>
