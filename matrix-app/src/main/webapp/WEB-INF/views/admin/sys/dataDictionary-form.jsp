﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
</head>
<body>
<div class="ibox-content">
		<form class="form-horizontal" id="dataform"
			onsubmit="javascripr:return false;">
			<input type="hidden" name="tokenUrl" value="${tokenUrl}">	 
			<input type="hidden" name="token" value="${token}">
		<c:if test="${obj ne null }">
			<input type="hidden" name="id" value="${obj.id }">
		</c:if>
		<div class="form-group">
			<label class="col-sm-2 control-label">数据类型名称<span class="text-danger">*</span></label>
			<div class="col-sm-4">
				<input type="text" dataType="*1-500" class="form-control"
					value="<c:out value="${obj.typeName }"></c:out>" name="typeName" nullmsg="数据类型名称不能为空">
			</div>
			<label class="col-sm-2 control-label">类型码<span class="text-danger">*</span></label>
			<div class="col-sm-4">
				<input type="text"   class="form-control" name="typeCode"
					value="<c:out value="${obj.typeCode }" ></c:out>" datatype="*1-32" nullmsg="类型码不能为空">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">字典值<span class="text-danger">*</span></label>
			<div class="col-sm-4">
				<input type="text" dataType="*1-50" class="form-control"
					value="<c:out value="${obj.value }"></c:out>" name="value" nullmsg="字典值不能为空">
			</div>
			<label class="col-sm-2 control-label">父码 </label>
			<div class="col-sm-4">
				<input type="text"   class="form-control" name="parentId"
					value="<c:out value="${obj.parentId }" ></c:out>" datatype="*1-32" ignore="ignore" >
			</div>
		</div>
		 <div class="form-group">
			<label class="col-sm-2 control-label">序号 </label>
			<div class="col-sm-4">
				<input type="text" dataType="n" class="form-control" ignore="ignore"
					value="<c:out value="${obj.sort }"></c:out>" name="sort" >
			</div>
			<label class="col-sm-2 control-label">扩展字段 </label>
			<div class="col-sm-4">
				<input type="text"   class="form-control" name="extValue" maxlength="40000"
					value="<c:out value="${obj.extValue }" ></c:out>"   ignore="ignore" >
			</div>
		</div>
		<div class="form-group ">
				<div class="col-sm-12 text-center">
				<a href="javascript:;" onclick="myForm.submit()"
					class="btn btn-success radius"><i class="fa fa-check"></i>  保存</a>&nbsp;&nbsp;&nbsp;&nbsp; <a
					 class="btn btn-danger radius" href="javascript:;" onclick="MTools.closeForm()" ><i class="fa fa-close"></i> 关闭</a>
			</div>
			</div>
	</form>
	</div>
</body>
<script type="text/javascript"
	src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
	
	var invokeUrl="${path}/do/admin/dataDictionary/addDataDictionary";
	<c:if test="${obj ne null }">
	invokeUrl = "${path}/do/admin/dataDictionary/modifyDataDictionary";
	</c:if>
	
	var myForm=MForm.initForm({
		invokeUrl:invokeUrl,
		afterSubmit:function(){
			parent.myGrid.serchData();
		},
	});
	</script>
</body>
</html>