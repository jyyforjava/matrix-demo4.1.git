﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="../images/favicon.ico">
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>


<title></title>
</head>
<body class="gray-bg">
	<div class="ibox-content" style="height: 100%">
		<div style="margin-top:50px"></div>
		<div class="row ">
			<div class="col-sm-6">

				<form class="form-horizontal" id="dataform"
					onsubmit="javascripr:return false;">
					<div class="form-group">
						<label class="col-sm-4 control-label">旧密码<span
							class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="password" datatype="*3-16" nullmsg="3-16位字符串"
								class="form-control text-muted" name="oldPassword">
							<div class="Validform_checktip"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">新密码<span
							class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="password" dataType="*1-20" class="form-control"
								name="newPassword">
							<div class="Validform_checktip"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4 control-label">确认密码<span
							class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input class="form-control" type="password" value=""
								name="userpassword2" datatype="*" recheck="newPassword"
								errormsg="您两次输入的账号密码不一致！">
							<div class="Validform_checktip"></div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-12 text-center">
							<a href="javascript:;" onclick="myForm.submit()"
								class="btn btn-success radius">确认修改</a>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>

	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
		var myForm = MForm.initForm({
			invokeUrl : "${path}/do/admin/updatePassword",
		});
	</script>
</body>
</html>
