<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 搜索框部分start -->
		<div class="row">
			<div class="col-sm-12">
				<form class="form-inline" id="serchform">
					<div class="form-group mr-20">
						<label class=" control-label">账号</label> <input
							name="userAccount" type="text" class="form-control">
					</div>
					<div class="form-group mr-20">
						<label class=" control-label">创建日期</label> <input
							name="beginTime" type="text" placeholder="开始时间"
							class="form-control datetimepicker" id="beginTime"> <input
							placeholder="结束时间" name="endTime" type="text"
							class="form-control datetimepicker" id="endTime">
					</div>
					<div class="form-group mr-20">
						<button onclick="myGrid.serchData(1)" type="button"
							class="btn btn-info">
							<i class="fa fa-search "></i> 搜索
						</button>
						<button type="reset" class="btn btn-info ">
							<i class="fa fa-refresh "></i> 重置
						</button>
					</div>
				</form>
			</div>
		</div>
		<!-- 搜索框部分en -->
		<div class="ibox-content radius-5 mt-5 mpanel">

			<div class="row">
				<div class="col-sm-12">
					<div class="option-bar"></div>
					<table id="mgrid">
						<thead>
							<tr>
								<th data-formatter="MGrid.indexfn" data-align="center"
									data-width="30px">序号</th>
								<th data-field="userAccount">登录账号</th>
								<th data-field="lrLoginTime" data-sortable="true"
									data-formatter="MGrid.getTime">登录时间</th>
								<th data-field="lrIp">登录IP</th>
								<th data-field="lrResult" data-formatter="status">登录状态</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
		var myGrid;
		$(function() {
			myGrid = MGrid.initGrid({
				url : "${path}/do/admin/showLoginRecordList",
				sortName : "lrLoginTime",
				sortOrder : "desc",
			});

			var _initParam = {
				format : 'yyyy-mm-dd hh:ii',
				todayBtn : true,
				autoclose : true,
				startView : 2,
				maxView : 3,
				minView : 0
			};
			MTools.ininDatetimepicker(_initParam);
			//限制结束时间不小于开始时间
			var initParam = {
				format : 'yyyy-mm-dd hh:ii',
				todayBtn : true,
				autoclose : true,
				startView : 2,
				maxView : 3,
				minView : 0
			};
			MTools.limitStartEndTime(initParam);

		});

		function status(value, row, index) {
			if (value == 1) {
				return '<span class="label label-success">成功</span>';
			} else if (value == 2) {
				return '<span class="label label-danger">失败</span>';
			}
		}
	</script>
</body>
</html>
