/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50529
Source Host           : localhost:3306
Source Database       : matrix

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2018-07-19 10:42:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for biz_repair_cost
-- ----------------------------
DROP TABLE IF EXISTS `biz_repair_cost`;
CREATE TABLE `biz_repair_cost` (
  `create_by` varchar(100) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(100) NOT NULL COMMENT '更新人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `repair_id` varchar(32) NOT NULL COMMENT '主键',
  `repair_factory` varchar(100) DEFAULT NULL COMMENT '维修厂',
  `repair_time` datetime DEFAULT NULL COMMENT '维修时间',
  `repair_car_num` varchar(32) DEFAULT NULL COMMENT '车牌号',
  `car_id` varchar(32) DEFAULT NULL COMMENT '车辆id',
  `repair_cost` decimal(10,2) DEFAULT NULL COMMENT '维修费用',
  `repair_handle_person` varchar(100) DEFAULT NULL COMMENT '经手人',
  `repair_item_detail` varchar(2000) DEFAULT NULL COMMENT '项目及费用明细',
  `repair_mileage` varchar(255) DEFAULT NULL COMMENT '公里数',
  PRIMARY KEY (`repair_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='(维修保养费用)';

INSERT INTO `biz_repair_cost` (`create_by`, `create_time`, `update_by`, `update_time`, `repair_id`, `repair_factory`, `repair_time`, `repair_car_num`, `car_id`, `repair_cost`, `repair_handle_person`, `repair_item_detail`, `repair_mileage`) VALUES ('c53a8fd276324d0283ac6e0d4c14c6c9', '2018-06-20 10:12:19', 'c53a8fd276324d0283ac6e0d4c14c6c9', '2018-06-20 10:12:19', '2cb8655c13894d89b915a2b92e373c65', '长沙达州维修厂', '2018-06-20 10:00:00', '湘A65378', '275819d5c52142cd82a5100239fb74f6', '2000.00', '李成江', '机油（50），空气扇（300）', '500');
INSERT INTO `biz_repair_cost` (`create_by`, `create_time`, `update_by`, `update_time`, `repair_id`, `repair_factory`, `repair_time`, `repair_car_num`, `car_id`, `repair_cost`, `repair_handle_person`, `repair_item_detail`, `repair_mileage`) VALUES ('c53a8fd276324d0283ac6e0d4c14c6c9', '2018-06-20 10:16:17', 'c53a8fd276324d0283ac6e0d4c14c6c9', '2018-06-20 10:16:17', '5cf8ca4538b442c68f4ff23653cf177b', '长沙老三维修厂', '2018-06-10 10:00:00', '湘C890M8', '2cc4a6526517472abffb460b51a80719', '200.00', '成果', '机油（30），滤芯（300）', '1000');
INSERT INTO `biz_repair_cost` (`create_by`, `create_time`, `update_by`, `update_time`, `repair_id`, `repair_factory`, `repair_time`, `repair_car_num`, `car_id`, `repair_cost`, `repair_handle_person`, `repair_item_detail`, `repair_mileage`) VALUES ('372e254f170a4e8d903052fed16e5e84', '2018-07-10 16:36:04', '372e254f170a4e8d903052fed16e5e84', '2018-07-10 16:36:04', '833f599935e14ef7982c2f21adf4040c', 'xx', '2018-07-10 16:00:00', '贵HFV790', '2cc4a6526517472abffb460b51a80719', '44.00', 'xx', 'ddd', '147');

