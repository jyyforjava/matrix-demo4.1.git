/*
Navicat MySQL Data Transfer

Source Server         : fc
Source Server Version : 50722
Source Host           : 120.78.163.100:3306
Source Database       : db_fcyx_dev

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-08-02 14:17:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `data_dictionary`;
CREATE TABLE `data_dictionary` (
  `create_by` varchar(100) NOT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(100) NOT NULL COMMENT '更新人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `id` varchar(32) NOT NULL COMMENT '主键',
  `type_name` varchar(500) DEFAULT NULL COMMENT '数据类型名称',
  `type_code` varchar(32) DEFAULT NULL COMMENT '类型码',
  `value` varchar(50) DEFAULT NULL COMMENT '字典值',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '父码id',
  `sort` int(11) DEFAULT NULL COMMENT '序号',
  `ext_value` varchar(4000) DEFAULT NULL COMMENT '扩展字段',
  `company_id` varchar(32) DEFAULT NULL COMMENT '所属公司id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='(数据字典)';

INSERT INTO `data_dictionary` (`create_by`, `create_time`, `update_by`, `update_time`, `id`, `type_name`, `type_code`, `value`, `parent_id`, `sort`, `ext_value`, `company_id`) VALUES ('system', '2018-08-02 14:21:39', 'system', '2018-08-02 14:21:46', '33464', '邮件模板', 'email_template', '', NULL, NULL, '<h3>尊敬的${name}，您好：</h3><p>&nbsp; 欢迎注册，您的验证是${code}。</p>', NULL);

