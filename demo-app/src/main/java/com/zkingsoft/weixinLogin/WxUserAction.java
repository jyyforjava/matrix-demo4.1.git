package com.zkingsoft.weixinLogin;


import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.constance.AppConstance;
import com.zkingsoft.common.constance.AppMessageCode;
import com.zkingsoft.common.tools.Constants;
import com.zkingsoft.common.tools.HttpCurlUtil;



/**
 * @description 微信接口
 * @author HEMING
 * @email 910000889@qq.com
 * @date 2018-04-11
 */
@RequestMapping(value = "wxUser")
@Controller
public class WxUserAction extends BaseAction {
	
	
	/**
	 * 
	 * @Desoription  微信公众号登录接口
	 * @author  何明
	 * @param  参数
	 * @return  返回值类型
	 * @date  2017年06月29日
	 */
	@RequestMapping(value="/wxlogin")
	public ModelAndView wxlogin(HttpServletRequest request,Integer reqType) throws Exception {
		String timestamp = String.valueOf(System.currentTimeMillis());
		request.getSession().setAttribute("token", timestamp);
		WebUtil.getSession().setAttribute("reqType", reqType);
		//替换域名
		String homeUrl=Constants.HOME_URL.replace("${ym}", "yd.xyz.cn");
		String url = Constants.OAUTH_AUTHORIZE_URL.replace("APPID", Constants.APPID)
				.replace("REDIRECT_URI", homeUrl)
				.replace("SCOPE", "snsapi_userinfo")
				.replace("STATE", timestamp);
		return new ModelAndView("redirect:"+url);
	}
	/**
	 * 
	 * @Desoription  微信公众号登录获取用户信息
	 * @author  何明
	 * @param  参数
	 * @return  返回值类型
	 * @date  2017年06月29日
	 */
	@RequestMapping(value="/home")
	public ModelAndView home(HttpServletRequest request,Model model) throws Exception {
	
		Integer reqType=(Integer) WebUtil.getSessionAttribute("reqType");
		LogUtil.debug("home method start!!");
		String code = request.getParameter("code");
		LogUtil.debug("code === " + code);
		JSONObject userJson;
		String userSubscribe ="";
		//请求微信服务器，获取openId和access_token
		String openIdUrl = Constants.OAUTH_ACCESS_TOKEN_URL
				.replace("APPID", Constants.APPID)
				.replace("SECRET", Constants.APPSECRET)
				.replace("CODE", code);
		LogUtil.debug("openIdUrl request start!");
		String result = HttpCurlUtil.sendGetHttp(openIdUrl, null);
		LogUtil.debug("openIdUrl request end! + json : {}",result);
		JSONObject json = JSONObject.fromObject(result);
		if (json.has("errcode") ) {
			LogUtil.debug("获取openId出错! errcode!");
			throw new GlobleException(AppMessageCode.User.LOGIN_TIMEOUT,"");
		}
		//获取微信唯一标识openId
		String  openId = json.getString("openid");
		String token = json.getString("access_token");
		LogUtil.debug("openId:{}" + openId );
		//获取acctoken
		if(reqType!=null ){//请求来自自定义菜单
			//请求微信服务器，重新获取access_token
			openIdUrl = Constants.GET_ACCESS_TOKEN_WITHOUTAUTH.replace("APPID", Constants.APPID)
					.replace("APPSECRET", Constants.APPSECRET);
			result = HttpCurlUtil.sendGetHttp(openIdUrl, null);
			json = JSONObject.fromObject(result);
			LogUtil.debug("json ===== {}" ,json);
			if (json.has("errcode") ) {
				LogUtil.debug("获取openId出错! errcode!");
				throw new GlobleException(AppMessageCode.User.LOGIN_TIMEOUT,"");
			}
			token = json.getString("access_token");
			
			//请求微信服务器，获取用户昵称、头像等信息
			String tokenUrl = Constants.GET_USER_INFO.replace("ACCESSTOKEN", token)
					.replace("OPENID", openId);
			result = HttpCurlUtil.sendGetHttp(tokenUrl, null);
			userJson = JSONObject.fromObject(result);
			LogUtil.debug("userJson ===== {}" ,json);
			
			//是否订阅公众号
			userSubscribe = userJson.getString("subscribe");
			
		}else{//请求来自其它页面,如：分享
			//请求微信服务器，获取用户信息
			String tokenUrl = Constants.GET_UESER_INFO.replace("ACCESS_TOKEN", token)
					.replace("OPENID", openId);
			result = HttpCurlUtil.sendGetHttp(tokenUrl, null);
			userJson = JSONObject.fromObject(result);
			LogUtil.debug("userJson ===== {}" ,json);
		}
		//微信昵称
		String userNickname = userJson.getString("nickname");
		//性别
		String sex = userJson.getString("sex");
		//微信头像
		String userWxImage = userJson.getString("headimgurl");
		/**
		 * 具体业务代码
		 * 1:根据openId查询用户信息,不存在就新增用户
		 * 2：将用户对象存入Session：WebUtil.setSessionAttribute(AppConstance.WEB_LOGIN_USER, bizUser);
		 */
		//根据请求值，跳转对应页面
		if(reqType==1){
		 //房间管理页面
		 return new ModelAndView("redirect:http://yd.xyz.cn/font/1.html");
		}
		return new ModelAndView("redirect:http://yd.xyz.cn/font/2.html");
	}
	
	
}
