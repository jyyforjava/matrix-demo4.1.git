package com.zkingsoft.sendsmsDemo;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.tools.excl.ExcelSheetPO;
import com.matrix.core.tools.excl.ExcelUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.SendsmsResult;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.tools.Sendsms;
import com.zkingsoft.excelDemo.bean.BizRepairCost;
import com.zkingsoft.excelDemo.dao.BizRepairCostDao;





/**
 * @description 
 * @author 何明
 * @date 2018-06-19 15:12
 */
@Controller
@RequestMapping(value = "admin/sendSms")
public class BizSendsmsAction extends BaseAction{

	
	//记录编辑前的值Before_Edit_Value
	public static final String BEV="BizRepairCost_BEV";

	
	/**
	 * 发送短信验证码
	 */
	@RequestMapping(value =  "/send")
	public @ResponseBody AjaxResult send(String tel) {
		SendsmsResult res=Sendsms.sendSms(tel);
		if(res.isSuccess()){
			return new AjaxResult(AjaxResult.STATUS_SUCCESS,"发送成功");
		}
		return new AjaxResult(AjaxResult.STATUS_FAIL,"发送失败");
		
	}
	
	
}