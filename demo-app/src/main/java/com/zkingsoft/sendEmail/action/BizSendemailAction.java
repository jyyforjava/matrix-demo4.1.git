package com.zkingsoft.sendEmail.action;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.PropertiesUtil;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.tools.excl.ExcelSheetPO;
import com.matrix.core.tools.excl.ExcelUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.DataDictionary;
import com.zkingsoft.common.bean.SendsmsResult;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.dao.DataDictionaryDao;
import com.zkingsoft.common.tools.Sendsms;
import com.zkingsoft.excelDemo.bean.BizRepairCost;
import com.zkingsoft.excelDemo.dao.BizRepairCostDao;
import com.zkingsoft.sendEmail.model.MailSenderInfo;

/**
 * @description 
 * @author 何明
 * @date 2018-06-19 15:12
 */
@Controller
@RequestMapping(value = "sendEmail")
public class BizSendemailAction extends BaseAction{
	@Autowired
	private DataDictionaryDao dataDictionaryDao;
	
	/**
	 * 发送邮件
	 */
	@RequestMapping(value =  "/send")
	public @ResponseBody AjaxResult send() {
		 // 这个类主要是设置邮件  
        MailSenderInfo mailInfo = new MailSenderInfo();  
        mailInfo.setMailServerHost(PropertiesUtil.getString("mail.smtp.host"));   //地址
        mailInfo.setMailServerPort(PropertiesUtil.getString("mail.smtp.port"));  //端口
        mailInfo.setValidate(Boolean.parseBoolean(PropertiesUtil.getString("mail.smtp.auth")));// 是否需要验证    
        mailInfo.setUserName(PropertiesUtil.getString("mail.userName")); // 实际发送者  
        mailInfo.setPassword(PropertiesUtil.getString("mail.password"));// 您的邮箱密码  
        mailInfo.setFromAddress(PropertiesUtil.getString("mail.userName")); // 设置发送人邮箱地址  
        mailInfo.setToAddress("910000889@qq.com"); // 设置接受者邮箱地址  
        //查询邮件模板
        List<DataDictionary> list=
         dataDictionaryDao.selectByType("email_template");
        if(CollectionUtils.isNotEmpty(list)){
        	 mailInfo.setSubject(list.get(0).getValue());  
        	 //替换模板内容
        	 String content=list.get(0)
        			 .getExtValue()
        			 .replace("${name}", "何明")
        			 .replace("${code}", "1234");
            LogUtil.debug("content:{}", content);
        	 mailInfo.setContent(content);  
             // 这个类主要来发送邮件  
        	// SendEmail.sendTextMail(mailInfo); // 发送文体格式  
            boolean result= SendEmail.sendHtmlMail(mailInfo); // 发送html格式  
            if(result){
            	return new AjaxResult(AjaxResult.STATUS_SUCCESS,"发送成功");
            }
        }
		return new AjaxResult(AjaxResult.STATUS_FAIL,"发送失败");
		
	}
	
	
}