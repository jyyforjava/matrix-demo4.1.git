package com.zkingsoft.common.bean;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

/**
 * @description (数据字典)
 * @author 姜友瑶
 * @date 2018-07-10 07:52
 */
public class DataDictionary  extends EntityDTO{
	@Extend
	private static final long serialVersionUID = 1L; 

	
	/**
	 * 主键
	 */
	private String  id;
			
	
	/**
	 * 数据类型名称
	 */
	private String  typeName;
			
	
	/**
	 * 类型码
	 */
	private String  typeCode;
			
	
	/**
	 * 字典值
	 */
	private String  value;
			
	
	/**
	 * 父码id
	 */
	private String  parentId;
			
	
	/**
	 * 序号
	 */
	private Integer  sort;
			
	
	/**
	 * 扩展字段
	 */
	private String  extValue;
			
	
	/**
	 * 所属公司id
	 */
	private String  companyId;
			
	

	public String getId() {
		return id;
	}
   	
   	public void setId(String id) {
		this.id=id;
	}
   	

	public String getTypeName() {
		return typeName;
	}
   	
   	public void setTypeName(String typeName) {
		this.typeName=typeName;
	}
   	

	public String getTypeCode() {
		return typeCode;
	}
   	
   	public void setTypeCode(String typeCode) {
		this.typeCode=typeCode;
	}
   	

	public String getValue() {
		return value;
	}
   	
   	public void setValue(String value) {
		this.value=value;
	}
   	

	public String getParentId() {
		return parentId;
	}
   	
   	public void setParentId(String parentId) {
		this.parentId=parentId;
	}
   	

	public Integer getSort() {
		return sort;
	}
   	
   	public void setSort(Integer sort) {
		this.sort=sort;
	}
   	

	public String getExtValue() {
		return extValue;
	}
   	
   	public void setExtValue(String extValue) {
		this.extValue=extValue;
	}
   	

	public String getCompanyId() {
		return companyId;
	}
   	
   	public void setCompanyId(String companyId) {
		this.companyId=companyId;
	}
   	


  
}