package com.zkingsoft.common.bean;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

public class EntityDTOExt extends EntityDTO {

	private static final long serialVersionUID = 1L;

	/**
	 * 起点
	 */
	@Extend
	private Integer offset = 0;
	/**
	 * 查询条数
	 */
	@Extend
	private Integer limit = 10;

	/**
	 * 排序方式
	 */
	@Extend
	private String order = "desc";
	/**
	 * 排序字段
	 */
	@Extend
	private String sort = "createTime";

	/**
	 * 用户接收界面id参数
	 */
	@Extend
	private List<String> ids;

	/**
	 * 是否选中全部 1，是 2不是
	 */
	@Extend
	private int isSelectAll;

	/**
	 * 判断界面是否传入了参数选中全部
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年1月30日
	 * @return
	 */
	public boolean isSelectAll() {
		return isSelectAll == 1;
	}

	public int getIsSelectAll() {
		return isSelectAll;
	}

	public void setIsSelectAll(int isSelectAll) {
		this.isSelectAll = isSelectAll;
	}

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getOrder() {
		if (!"desc".equals(order) && !"asc".equals(order)) {
			return "desc";
		}
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		if (StringUtils.isBlank(sort)) {
			sort = "create_time";
		}
		return propertyToColumn(sort);
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String propertyToColumn(String property) {
		if (property == null) {
			return null;
		}
		StringBuffer columnName = new StringBuffer(property);
		for (int i = 0; i < columnName.length(); i++) {
			if (columnName.charAt(i) >= 'A' && columnName.charAt(i) <= 'Z') {
				String upcase = "_" + (columnName.charAt(i) + "").toLowerCase();
				columnName.replace(i, i + 1, upcase);
			}
		}
		return columnName.toString();
	}

}
