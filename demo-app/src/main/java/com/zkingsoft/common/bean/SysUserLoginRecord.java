package com.zkingsoft.common.bean;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

/**
 * 用户登录记录
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月12日
 */
public class SysUserLoginRecord extends EntityDTO{
	
	@Extend
	private static final long serialVersionUID = 1L;
 

	private String lrId;

	/**
	 * 用户id
	 */
	private String userAccount;

	/**
	 * 登录结果 1.登录成功 2.密码错误，3.其他
	 */
	private Integer lrResult;

	/**
	 * 日志内容
	 */
	private String lrRemark;

	/**
	 * 使用ip
	 */

	private String lrIp;
	/**
	 * 删除标志
	 */
	private String lrValid;
	/**
	 * 创建时间
	 */
	private Date lrLoginTime;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	private Date beginTime;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	private Date endTime;
	
	
  
	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getLrId() {
		return lrId;
	}

	public void setLrId(String lrId) {
		this.lrId = lrId;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public Integer getLrResult() {
		return lrResult;
	}

	public void setLrResult(Integer lrResult) {
		this.lrResult = lrResult;
	}

	public String getLrRemark() {
		return lrRemark;
	}

	public void setLrRemark(String lrRemark) {
		this.lrRemark = lrRemark;
	}

	public String getLrIp() {
		return lrIp;
	}

	public void setLrIp(String lrIp) {
		this.lrIp = lrIp;
	}

	public Date getLrLoginTime() {
		return lrLoginTime;
	}

	public void setLrLoginTime(Date lrLoginTime) {
		this.lrLoginTime = lrLoginTime;
	}

	public String getLrValid() {
		return lrValid;
	}

	public void setLrValid(String lrValid) {
		this.lrValid = lrValid;
	}
 

	 
	
	
 
}