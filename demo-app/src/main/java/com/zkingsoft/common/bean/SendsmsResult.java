package com.zkingsoft.common.bean;

public class SendsmsResult {
	
	/**
	 * 返回值为 2 时，表示提交成功
	 */
	private String code;
	
	/**
	 * 提交结果描述
	 */
	private String msg;
	
	/**
	 * 当提交成功后，此字段为流水号，否则为 0
	 */
	private String smsid;
	
	/**
	 * 提交的手机号码
	 */
	private String tel;
	
	/**
	 * 发送的验证码
	 */
	private String smsCode;
	
	/**
	 * 提交结果：true(成功)
	 */
	private boolean success=false;



	public String getSmsCode() {
		return smsCode;
	}

	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSmsid() {
		return smsid;
	}

	public void setSmsid(String smsid) {
		this.smsid = smsid;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		
		return "{SendsmsResult:{"
				+"code:"+code+","
				+"smsid:"+smsid+","
				+"msg:"+msg+","
				+"tel:"+tel+","
				+"smsCode:"+smsCode+","
				+"success:"+success+"}}";
	}
	
}
