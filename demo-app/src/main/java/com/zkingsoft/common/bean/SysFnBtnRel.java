package com.zkingsoft.common.bean;

import com.matrix.core.anotations.Extend;
import com.matrix.core.pojo.EntityDTO;

/**
 * 功能按钮关系表
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月3日
 */
public class SysFnBtnRel extends EntityDTO {

	@Extend
	private static final long serialVersionUID = 1L;
	
	/** 主键:这个关系表物理主键在修改比较中不参与比较 **/
	@Extend
	private String fbId;
	/** 功能id **/
	private String fnId;
	/** 按钮value **/
	private String btnValue;
	/** 按钮value **/
	private String btnKey;
	/** 访问路径 **/
	private String fbPath;

	
	/**
	 * 在权限分配时用于标记是否拥有该项功能
	 */
	@Extend
	private boolean hasThisBtn = false;
	
	public boolean isHasThisBtn() {
		return hasThisBtn;
	}

	public void setHasThisBtn(boolean hasThisBtn) {
		this.hasThisBtn = hasThisBtn;
	}

	public String getFbId() {
		return fbId;
	}

	public void setFbId(String fbId) {
		this.fbId = fbId;
	}

	public String getFnId() {
		return fnId;
	}

	public void setFnId(String fnId) {
		this.fnId = fnId;
	}

	public String getBtnValue() {
		return btnValue;
	}

	public void setBtnValue(String btnValue) {
		this.btnValue = btnValue;
	}

	public String getFbPath() {
		return fbPath;
	}

	public void setFbPath(String fbPath) {
		this.fbPath = fbPath;
	}

	public String getBtnKey() {
		return btnKey;
	}

	public void setBtnKey(String btnKey) {
		this.btnKey = btnKey;
	}
}
