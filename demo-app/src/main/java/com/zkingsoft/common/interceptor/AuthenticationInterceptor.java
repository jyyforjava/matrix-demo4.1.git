package com.zkingsoft.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.tools.WebUtil;

/**
 * 身份认证拦截器
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月8日
 */
public class AuthenticationInterceptor implements HandlerInterceptor {

	private static final String LOGIN_TIME_OUT = "loginTimeOut...";
	private static final String X_REQUESTED_WITH = "X-Requested-With";
	private static final String DO_COMMON_REDIRECT_LOGIN = "/do/common/toLogin";
	private static final String DEVELOPER = "/developer/";
	private static final String CUSTOMER = "/customer/";
	private static final String SUPER = "/super/";
	private static final String ADMIN = "/admin/";
	/**
	 * 检查管理员是否登陆
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {

		String requestUrl = request.getRequestURI();
		// 如果访问特殊的路径需要验证管理员的登录权限
		if (requestUrl.indexOf(ADMIN) != -1 || requestUrl.indexOf(SUPER) != -1
				|| requestUrl.indexOf(DEVELOPER) != -1 || requestUrl.indexOf(CUSTOMER) != -1) {
			if (WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY) == null) {
				// 判断是否为异步请求
				String requestType = request.getHeader(X_REQUESTED_WITH);
				if (requestType == null) {
					response.sendRedirect(request.getContextPath() + DO_COMMON_REDIRECT_LOGIN);
				} else {
					response.getWriter().write(LOGIN_TIME_OUT);
				}
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
	}
}
