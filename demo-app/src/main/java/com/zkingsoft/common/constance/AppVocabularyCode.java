package com.zkingsoft.common.constance;

/**
 * 应用名词code
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月28日
 */
public interface AppVocabularyCode {

	/** 功能 **/
	String FUNCTION = "4000_0000";
	/** 按钮 **/
	String BUTTON = "4000_0001";
	/** 公司 **/
	String COMPANY = "4000_0002";
	/** 管理员 **/
	String ADMIN = "4000_0003";
	/** 角色 **/
	String ROLE = "4000_0004";

}
