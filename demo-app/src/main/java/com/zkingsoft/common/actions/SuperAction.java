package com.zkingsoft.common.actions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.anotations.RemoveRequestToken;
import com.matrix.core.anotations.SaveRequestToken;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.constance.SystemMessageCode;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.constance.AppConstance;
import com.zkingsoft.common.constance.AppVocabularyCode;
import com.zkingsoft.common.service.SysUsersService;

/**
 * @description 管理员总action
 * @author 姜友瑶
 * @email 935090232@qq.com
 * @date 2016-06-26
 */
@Controller
@RequestMapping(value = "super")
public class SuperAction extends BaseAction {
	@Autowired
	private SysUsersService sysUsersService;

	public static final String BEV = "SYSUSERS_BEV";

	/**
	 * 
	 * @Description: 页面定向方法，每个权限模块公用一个，每个模块共享一个一级路径，已便于进行权限过滤
	 * @author:姜友瑶
	 * @param page1
	 * @param page2
	 * @return 返回类型 String
	 * @date 2016年8月31日
	 */
	@RequestMapping(value = "/redirect/{page1}/{page2}")
	public String redirect(@PathVariable("page1") String page1, @PathVariable("page2") String page2) {
		return "super/" + page1 + "/" + page2;
	}

	/**
	 * 
	 * @Description: 页面定向方法，每个权限模块公用一个，每个模块共享一个一级路径，已便于进行权限过滤
	 * @author:姜友瑶
	 * @param page1
	 * @param page2
	 * @return 返回类型 String
	 * @date 2016年8月31日
	 */
	@RequestMapping(value = "/redirect/{page1}")
	public String redirect(@PathVariable("page1") String page1) {
		return "super/" + page1;
	}

	/**
	 * 新增超级管理员
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysUsers
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value = "/addSuperUser")
	public @ResponseBody AjaxResult addSuperUser(SysUsers sysUsers) {
		int i = sysUsersService.addCompanySuper(sysUsers);
		if (i > 0) {
			return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.ADD_SUCCES, AppVocabularyCode.ADMIN);
		} else {
			throw new GlobleException(SystemErrorCode.DATA_ADD_FAIL);
		}
	}

	/**
	 * 修改超级管理员
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysUsers
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value = "/modifySuperUser")
	public @ResponseBody AjaxResult modifySuperUser(SysUsers sysUsers) {
		AjaxResult result = modify(sysUsersService, WebUtil.getSessionAttribute(BEV), sysUsers,
				AppVocabularyCode.ADMIN);
		WebUtil.removeSessionAttribute(BEV);
		return result;
	}

	/**
	 * 进入修改界面
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param id
	 * @return
	 */
	@SaveRequestToken
	@RequestMapping(value = "/editForm")
	public String editForm(String id) {
		SysUsers sysUsers;
		if (id != null) {
			sysUsers = sysUsersService.findById(id);
			WebUtil.getRequest().setAttribute("obj", sysUsers);
			WebUtil.setSessionAttribute(BEV, sysUsers);
		}
		return "super/super-form";
	}

	/**
	 * 列表显示
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysUsers
	 * @param pageVo
	 * @return
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(SysUsers sysUsers, PaginationVO pageVo) {
		sysUsers.setSuUserType(AppConstance.USER_TYPE_ADMIN);
		return showList(sysUsersService, sysUsers, pageVo);
	}

	/**
	 * 删除
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param keys
	 * @return
	 */
	@RequestMapping(value = "/del")
	public @ResponseBody AjaxResult del(String keys) {
		return remove(sysUsersService, keys);
	}

}