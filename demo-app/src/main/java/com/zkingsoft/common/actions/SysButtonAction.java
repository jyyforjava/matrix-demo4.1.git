package com.zkingsoft.common.actions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.anotations.RemoveRequestToken;
import com.matrix.core.anotations.SaveRequestToken;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.SysButton;
import com.zkingsoft.common.constance.AppVocabularyCode;
import com.zkingsoft.common.service.SysButtonService;

/**
 * 按钮管理控制器
 * 
 * @author jiangyouyao
 * @date 2016-11-17 16:43
 */
@Controller
@RequestMapping(value = "admin/sysBtn")
public class SysButtonAction extends BaseAction {

	@Autowired
	private SysButtonService sysButtonService;

	public static final String BEV = "SYSBUTTON_BEV";

	/**
	 * 列表显示
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysBtn
	 * @param pageVo
	 * @return
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(SysButton sysBtn, PaginationVO pageVo) {
		return showList(sysButtonService, sysBtn, pageVo);
	}

	/**
	 * 新增
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysBtn
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value = "/addButton")
	public @ResponseBody AjaxResult addButton(SysButton sysBtn) {
		return add(sysButtonService, sysBtn, AppVocabularyCode.BUTTON);
	}

	/**
	 * 修改
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysBtn
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value = "/modifyButton")
	public @ResponseBody AjaxResult modifyButton(SysButton sysBtn) {
		AjaxResult result = modify(sysButtonService, WebUtil.getSessionAttribute(BEV), sysBtn,
				AppVocabularyCode.BUTTON);
		WebUtil.removeSessionAttribute(BEV);
		return result;
	}

	/**
	 * 进入修改界面
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param id
	 * @return
	 */
	@SaveRequestToken
	@GetMapping(value = "/editForm")
	public String editForm(String id) {
		SysButton sysBtn;
		if (id != null) {
			sysBtn = sysButtonService.findById(id);
			WebUtil.getRequest().setAttribute("obj", sysBtn);
			WebUtil.setSessionAttribute(BEV, sysBtn);
		}
		return "developer/sysBtn-form";
	}

	/**
	 * 删除
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param keys
	 * @return
	 */
	@RequestMapping(value = "/del")
	public @ResponseBody AjaxResult del(String keys) {
		return remove(sysButtonService, keys);
	}

	/**
	 * 显示所有按钮
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @return
	 */
	@RequestMapping(value = "/all")
	public @ResponseBody AjaxResult all() {
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, sysButtonService.findByModel(null));
	}

}