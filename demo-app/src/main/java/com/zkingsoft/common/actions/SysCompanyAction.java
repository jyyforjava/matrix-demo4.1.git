package com.zkingsoft.common.actions;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.anotations.RemoveRequestToken;
import com.matrix.core.anotations.SaveRequestToken;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.SysCompany;
import com.zkingsoft.common.constance.AppVocabularyCode;
import com.zkingsoft.common.service.SysCompanyService;

/**
 * 公司管理
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月5日
 */
@Controller
@RequestMapping(value = "admin/sysCompany")
public class SysCompanyAction extends BaseAction {

	@Autowired
	private SysCompanyService sysCompanyService;

	public static final String BEV = "SYSCOMPANY_BEV";

	/**
	 * 列表显示
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysCompany
	 * @param pageVo
	 * @return
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(SysCompany sysCompany, PaginationVO pageVo) {
		return showList(sysCompanyService, sysCompany, pageVo);
	}

	/**
	 * 显示所有公司
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @return
	 */
	@RequestMapping(value = "/all")
	public @ResponseBody AjaxResult all() {
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, sysCompanyService.findByModel(null));
	}

	/**
	 * 新增公司
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysCompany
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value = "/addCompany")
	public @ResponseBody AjaxResult addCompany(SysCompany sysCompany) {
		return add(sysCompanyService, sysCompany, AppVocabularyCode.COMPANY);
	}

	/**
	 * 修改公司信息
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param sysCompany
	 * @return
	 */
	@RemoveRequestToken
	@PostMapping(value = "/modifyCompany")
	public @ResponseBody AjaxResult modifyCompany(SysCompany sysCompany) {
		AjaxResult result = modify(sysCompanyService, WebUtil.getSessionAttribute(BEV), sysCompany,
				AppVocabularyCode.COMPANY);
		WebUtil.removeSessionAttribute(BEV);
		return result;
	}

	/**
	 * 进入修改界面
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param id
	 * @return
	 */
	@SaveRequestToken
	@RequestMapping(value = "/editForm")
	public String editForm(String id) {
		SysCompany sysCompany;
		if (id != null) {
			sysCompany = sysCompanyService.findById(id);
			WebUtil.getRequest().setAttribute("obj", sysCompany);
			WebUtil.setSessionAttribute(BEV, sysCompany);
		}
		return "super/sysCompany-form";
	}

	/**
	 * 删除
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param keys
	 * @return
	 */
	@RequestMapping(value = "/del")
	public @ResponseBody AjaxResult del(String keys) {
		return remove(sysCompanyService, keys);
	}

}