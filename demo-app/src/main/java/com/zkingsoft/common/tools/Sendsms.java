package com.zkingsoft.common.tools;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.matrix.core.tools.LogUtil;
import com.zkingsoft.common.bean.SendsmsResult;
import com.zkingsoft.common.constance.AppConstance;

public class Sendsms {
	
	//互亿无线短信验证码，同一个号码一天最多发送5条短信
	private static String Url = "http://106.ihuyi.cn/webservice/sms.php?method=Submit";

	/**
	 * 发送并返回短信验证码
	  * @author HEMING
	  * @email 910000889@qq.com
	  * @date 2018年7月23日 
	 */
	public static SendsmsResult sendSms(String tel) {
		SendsmsResult result=new SendsmsResult();
		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod(Url);
		client.getParams().setContentCharset("UTF-8");
		method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
		//生成随机四位数验证码
		int mobile_code = (int) ((Math.random() * 9 + 1) * 1000);
		//短信模板
		String content = new String("您的验证码是【" + mobile_code + "】。请在页面中提交验证码完成验证。");
		//提交的数据：account为账号，password为密码，mobile为手机号码，content为发送的消息内容
		NameValuePair[] data = { 
				new NameValuePair("account", AppConstance.IHUYI_ACCOUNT), new NameValuePair("password", AppConstance.IHUYI_PASSWORD),
				new NameValuePair("mobile", tel), new NameValuePair("content", content), };
		method.setRequestBody(data);
		try {
			client.executeMethod(method);
			//获取返回结果
			String submitResult = method.getResponseBodyAsString();
			//返回结果为XML结构，转成实体类
			Document doc = DocumentHelper.parseText(submitResult);
			Element root = doc.getRootElement();
			result.setCode(root.elementText("code"));
			result.setMsg(root.elementText("msg"));
			result.setSmsid(root.elementText("smsid"));
			result.setTel(tel);
			if ("2".equals(result.getCode())) {
				//提交成功
				LogUtil.info("#短信验证码发送结果#{}", "提交成功");
				result.setSmsCode(mobile_code+"");
				result.setSuccess(true);
			}else{
				//提交失败
				LogUtil.info("#短信验证码发送结果#{}","提交失败");
				result.setSuccess(false);
			}
			LogUtil.info("#短信验证码发送结果#{}", result);
		} catch (IOException |DocumentException e) {
			LogUtil.error("#短信模块#发送异常", e, tel);
		} 
		return result;
	}
	
	
}