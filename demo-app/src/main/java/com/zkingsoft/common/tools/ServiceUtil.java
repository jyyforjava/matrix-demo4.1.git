package com.zkingsoft.common.tools;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matrix.core.constance.MatrixConstance;
import com.zkingsoft.common.dao.UtilDao;

/**
 * 
 * @Description: 提供一些特殊的sql操作
 * @author:姜友瑶
 * @date 2016年8月28日
 */
@Service("ServiceUtil")
public class ServiceUtil {
	@Autowired
	private UtilDao utilDao;

	/**
	 * 单字段判断去重 ß
	 * 
	 * @author jyy
	 * @time 2016-07-31
	 * @param tableName
	 *            表明
	 * @param column
	 *            字段名
	 * @param value
	 *            字段值
	 * @return
	 */
	public boolean addCheckRepeat(String tableName, String column, Object value) {
		HashMap<String, Object> query = excuteQuery(tableName, column, value);
		if (query != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 双字段去重
	 * 
	 * @author jyy
	 * @time 2016-07-31
	 * @param tableName
	 *            表名
	 * @param column1
	 *            字段名1
	 * @param value1
	 *            字段名1的值
	 * @param column2
	 *            字段名2
	 * @param value2
	 *            字段名2的值
	 * @return
	 */
	public boolean addCheckRepeatTowColumn(String tableName, String column1, Object value1, String column2,
			Object value2) {
		HashMap<String, Object> query = excuteTow(tableName, column1, value1, column2, value2);
		if (query != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 双字段去重 更新时调用
	 * 
	 * @author jyy
	 * @time 2016-07-31
	 * @param tableName
	 * @param column1
	 * @param value1
	 * @param column2
	 * @param value2
	 * @param idName
	 *            数据库id名称
	 * @param idValue
	 *            更新数据的id
	 * @return
	 */
	public boolean updateCheckRepeatTowColumn(String tableName, String column1, Object value1, String column2,
			Object value2, String idName, Object idValue) {
		HashMap<String, Object> query = excuteTow(tableName, column1, value1, column2, value2);
		return query != null && !query.get(idName).equals(idValue);
	}

	/**
	 * 单字段去重 更新时调用
	 * 
	 * @author jyy
	 * @time 2016-07-31
	 * @param tableName
	 * @param column
	 * @param value
	 * @param idName
	 *            数据库id名称
	 * @param idValue
	 *            更新数据的id
	 * @return
	 */
	public boolean updateCheckRepeat(String tableName, String column, Object value, String idName, Object idValue) {
		HashMap<String, Object> query = excuteQuery(tableName, column, value);
		return query != null && !query.get(idName).equals(idValue);
	}

	private HashMap<String, Object> excuteQuery(String tableName, String column, Object value) {
		HashMap<String, Object> query = new HashMap<>(MatrixConstance.COLLECTION_SIZE);
		query.put("tableName", tableName);
		query.put("column", column);
		query.put("value", value);
		query = (HashMap<String, Object>) utilDao.selectRepeat(query);
		return query;
	}

	private HashMap<String, Object> excuteTow(String tableName, String column1, Object value1, String column2,
			Object value2) {
		HashMap<String, Object> query = new HashMap<>(MatrixConstance.COLLECTION_SIZE);
		query.put("tableName", tableName);
		query.put("column1", column1);
		query.put("value1", value1);
		query.put("column2", column2);
		query.put("value2", value2);
		query = (HashMap<String, Object>) utilDao.selectRepeatTowColumn(query);
		return query;
	}

}
