package com.zkingsoft.common.tools;


public final class  Constants {
	
	public static String APPID = "wxd360da97317e";
	
	public static String APPSECRET = "ed21153252ed69fac25afc0dfb0b";
	
	public static String HOME_URL = "http%3A%2F%2F${ym}%2Fbs%2Fdo%2FwxUser%2Fhome";
	
	
	public static String OAUTH_AUTHORIZE_URL="https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
	
	public static String WEIXIN_LOGIN_URL = "https://open.weixin.qq.com/connect/qrconnect?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
	
	public static String OAUTH_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

	public static String GET_ACCESS_TOKEN_WITHOUTAUTH = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	
	public static String GET_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESSTOKEN&openid=OPENID&lang=zh_CN";
	
	public static String GETTICKET_URL="https://api.weixin.qq.com/cgi-bin/ticket/getticket";
	
	public static String GET_UESER_INFO="https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID";
	
	/*发送模板消息*/
	public static String TEMPLATEMSG_SEN = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
	/*发送消息（所有关注人）*/
	public static String SEND_ALL_URL="https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN";
	/*发送消息*/
	public static String SEND_URL="https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=ACCESS_TOKEN";
	/*删除已发送消息*/
	public static String DELETE_URL="https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=ACCESS_TOKEN";
	/*预览*/
	public static String PREVIEW_URL="https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=ACCESS_TOKEN";
	/*上传新闻*/
	public static String UPLOAD_NEWS="https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=ACCESS_TOKEN";
	/*上传微略图*/
	public static String UPLOAD_THUMB="http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=thumb";
	/*上传图片*/
	public static String UPLOAD_IMG="https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN";
	/*获取公众号已创建的标签*/
	public static String TAGS_GET="https://api.weixin.qq.com/cgi-bin/tags/get?access_token=ACCESS_TOKEN";
}
