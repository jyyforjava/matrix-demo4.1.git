package com.zkingsoft.common.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.ModelUtils;
import com.zkingsoft.common.bean.SysRolePwoerFn;
import com.zkingsoft.common.dao.SysRolePwoerFnDao;
import com.zkingsoft.common.service.SysRolePwoerFnService;

/**
 * 角色和功能的关系
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 11, 2017
 */
@Service
public class SysRolePwoerFnServiceImpl implements SysRolePwoerFnService {

	@Autowired
	private SysRolePwoerFnDao sysRolePwoerFnDao;

	@Override
	public int add(SysRolePwoerFn sysRolePwoerFn) {

		return sysRolePwoerFnDao.insert(sysRolePwoerFn);

	}

	@Override
	public int batchAdd(List<SysRolePwoerFn> sysRolePwoerFnList) {
		return sysRolePwoerFnDao.batchInsert(sysRolePwoerFnList);
	}

	@Override
	public int modifyByMap(SysRolePwoerFn oldSysRolePwoerFn, SysRolePwoerFn newSysRolePwoerFn) {
		Map<String, Object> modifyMap = null;
		try {
			if (!ModelUtils.isModified(oldSysRolePwoerFn, newSysRolePwoerFn)) {
				return 1;
			}
			modifyMap = ModelUtils.comparePojo2Map(oldSysRolePwoerFn, newSysRolePwoerFn);
		} catch (Exception e) {
			throw new GlobleException("数据对比失败");
		}
		if (modifyMap.size() > 0) {
			modifyMap.put("rpfId", oldSysRolePwoerFn.getRpfId());
			sysRolePwoerFnDao.updateByMap(modifyMap);
		}
		return 1;
	}

	@Override
	public int modifyByModel(SysRolePwoerFn sysRolePwoerFn) {

		return sysRolePwoerFnDao.updateByModel(sysRolePwoerFn);

	}

	@Override
	public int remove(List<String> list) {

		return sysRolePwoerFnDao.deleteByIds(list);

	}

	@Override
	public int removeById(String rpfId) {

		return sysRolePwoerFnDao.deleteById(rpfId);

	}

	@Override
	public int removeByModel(SysRolePwoerFn sysRolePwoerFn) {

		return sysRolePwoerFnDao.deleteByModel(sysRolePwoerFn);

	}

	@Override
	public List<SysRolePwoerFn> findInPage(SysRolePwoerFn sysRolePwoerFn, PaginationVO pageVo) {

		return sysRolePwoerFnDao.selectInPage(sysRolePwoerFn, pageVo);

	}

	@Override
	public List<SysRolePwoerFn> findByModel(SysRolePwoerFn sysRolePwoerFn) {

		return sysRolePwoerFnDao.selectByModel(sysRolePwoerFn);

	}

	@Override
	public int findTotal(SysRolePwoerFn sysRolePwoerFn) {

		return sysRolePwoerFnDao.selectTotalRecord(sysRolePwoerFn);

	}

	@Override
	public SysRolePwoerFn findById(String rpfId) {

		return sysRolePwoerFnDao.selectById(rpfId);

	}

}