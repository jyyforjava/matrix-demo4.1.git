package com.zkingsoft.common.service;

import java.util.List;

import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.web.BaseServices;
import com.zkingsoft.common.bean.SysUserLoginRecord;
import com.zkingsoft.common.bean.SysUsers;

/**
 * This field was generated by Zking.software.Codegen.
 * 
 * @date 2016-11-16 15:25
 */
public interface SysUsersService extends BaseServices<SysUsers> {

	/**
	 * 新增SysUsers
	 * 
	 */
	public int add(SysUsers sysUsers);

	/**
	 * 根据map键值对 更新SysUsers
	 * 
	 */
	public int modifyByMap(SysUsers oldSysUsers, SysUsers newSysUsers);

	/**
	 * 根据对象 更新SysUsers
	 * 
	 */
	public int modifyByModel(SysUsers sysUsers);

	/**
	 * 批量删除SysUsers
	 * 
	 */
	public int remove(List<String> list);

	/**
	 * 根据id删除SysUsers
	 * 
	 */
	public int removeById(String suId);

	/**
	 * 根据对象删除SysUsers
	 * 
	 */
	public int removeByModel(SysUsers sysUsers);

	/**
	 * 分页查询SysUsers
	 * 
	 */
	public List<SysUsers> findInPage(SysUsers sysUsers, PaginationVO pageVo);

	/**
	 * 根据对象查询SysUsers
	 * 
	 */
	public List<SysUsers> findByModel(SysUsers sysUsers);

	/**
	 * 统计记录数SysUsers
	 * 
	 */
	public int findTotal(SysUsers sysUsers);

	/**
	 * 根据id查询SysUsers
	 * 
	 */
	public SysUsers findById(String suId);

	/**
	 * 添加一个公司的超级管理管理员
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月5日
	 * @param sysUsers
	 * @return
	 */
	public int addCompanySuper(SysUsers sysUsers);

	public int addAdmin(SysUsers sysUsers);

	/**
	 * 锁住账号
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月12日
	 * @param suAccount
	 */
	public void lockUser(String suAccount);

	/**
	 * 解锁账号
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月12日
	 * @param suAccount
	 */
	public void unlockUser(String suAccount);

	/**
	 * 统计今天的登录失败次数
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月12日
	 * @param suAccount
	 * @return
	 */
	public int countUserTodayErrorLoginTimes(String suAccount);

	/**
	 * 添加一条登录记录
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月12日
	 * @param loginRecord
	 */
	public void addUserLoginRecord(SysUserLoginRecord loginRecord);

	/**
	 * 清除当天的登录失败次数
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月12日
	 * @param suAccount
	 */
	public void cleanUserTodayErrorLoginTime(String suAccount);

}