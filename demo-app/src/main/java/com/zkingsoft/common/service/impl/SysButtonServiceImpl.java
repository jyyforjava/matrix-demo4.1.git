package com.zkingsoft.common.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.ModelUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.zkingsoft.common.bean.SysButton;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.dao.SysButtonDao;
import com.zkingsoft.common.service.SysButtonService;
import com.zkingsoft.common.tools.ServiceUtil;

/**
 * 按钮管理
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 11, 2017
 */
@Service
public class SysButtonServiceImpl implements SysButtonService {

	private static final String BTN_ID = "btn_id";

	private static final String BTN_VALUE = "btn_value";

	@Autowired
	private SysButtonDao sysBtnDao;

	@Autowired
	private ServiceUtil serviceUtil;

	private final String tableName = "sys_button";

	@Override
	public int add(SysButton sysBtn) {

		if (serviceUtil.addCheckRepeat(tableName, BTN_VALUE, sysBtn.getBtnValue())) {
			throw new GlobleException(SystemErrorCode.DATA_REPEAT, sysBtn.getBtnValue());
		}
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		sysBtn.setCreateBy(user.getSuName());
		sysBtn.setUpdateBy(user.getSuName());
		sysBtn.setBtnId(UUIDUtil.getRandomID());
		return sysBtnDao.insert(sysBtn);

	}

	@Override
	public int modifyByMap(SysButton oldSysBtn, SysButton newSysBtn) {
		if (serviceUtil.updateCheckRepeat(tableName, BTN_VALUE, newSysBtn.getBtnValue(), BTN_ID,
				newSysBtn.getBtnId())) {
			throw new GlobleException(SystemErrorCode.DATA_REPEAT, newSysBtn.getBtnValue());
		}
		SysUsers user = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		newSysBtn.setUpdateBy(user.getSuName());
		Map<String, Object> modifyMap = null;
		try {
			if (!ModelUtils.isModified(oldSysBtn, newSysBtn)) {
				return MatrixConstance.DML_SUCCESSS;
			}
			modifyMap = ModelUtils.comparePojo2Map(oldSysBtn, newSysBtn);
		} catch (Exception e) {
			throw new GlobleException(SystemErrorCode.DATA_UPDATE_FAIL, e, newSysBtn.getBtnValue());
		}
		if (modifyMap.size() > 0) {
			modifyMap.put("btnId", oldSysBtn.getBtnId());
			return sysBtnDao.updateByMap(modifyMap);
		}  
		return MatrixConstance.DML_SUCCESSS;
	}

	@Override
	public int modifyByModel(SysButton sysBtn) {

		return sysBtnDao.updateByModel(sysBtn);

	}

	@Override
	public int remove(List<String> list) {

		return sysBtnDao.deleteByIds(list);

	}

	@Override
	public int removeById(String btnId) {

		return sysBtnDao.deleteById(btnId);

	}

	@Override
	public int removeByModel(SysButton sysBtn) {

		return sysBtnDao.deleteByModel(sysBtn);

	}

	@Override
	public List<SysButton> findInPage(SysButton sysBtn, PaginationVO pageVo) {

		return sysBtnDao.selectInPage(sysBtn, pageVo);

	}

	@Override
	public List<SysButton> findByModel(SysButton sysBtn) {

		return sysBtnDao.selectByModel(sysBtn);

	}

	@Override
	public int findTotal(SysButton sysBtn) {

		return sysBtnDao.selectTotalRecord(sysBtn);

	}

	@Override
	public SysButton findById(String btnId) {

		return sysBtnDao.selectById(btnId);

	}

}