package com.zkingsoft.common.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.matrix.core.pojo.PaginationVO;
import com.zkingsoft.common.bean.SysFnBtnRel;
import com.zkingsoft.common.bean.SysFunction;

/**
 * 功能的数据库操作类
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月4日
 */
public interface SysFunctionDao {

	public int insert(SysFunction sysFunction);

	public int batchInsert(@Param("list") List<SysFunction> sysFunctionList);

	public int updateByMap(Map<String, Object> modifyMap);

	public int updateByModel(SysFunction sysFunction);

	public int deleteByIds(@Param("list") List<String> list);

	public int deleteById(String fnId);

	public int deleteByModel(@Param("record") SysFunction sysFunction);

	public List<SysFunction> selectInPage(@Param("record") SysFunction sysFunction,
			@Param("pageVo") PaginationVO pageVo);

	public List<SysFunction> selectByModel(@Param("record") SysFunction sysFunction);

	public int selectTotalRecord(@Param("record") SysFunction sysFunction);

	public SysFunction selectById(String fnId);

	public SysFunction selectForUpdate(String fnId);

	/**
	 * 根据公司id查询功能
	 * 
	 * @param companyId
	 * @return
	 */
	public List<SysFunction> selectCompanyFunction(String companyId);

	public List<SysFunction> selectFunctionByRoleIds(String roleIds);

	public void batchChangeStatu(@Param("status") String status, @Param("list") List<String> list);

	/**
	 * 批量插入功能按钮的联系表
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月4日
	 * @param rels
	 */
	public void bathInsertFnButRel(List<SysFnBtnRel> rels);

	/**
	 * 删除功能所关联的按钮
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月4日
	 * @param fnId
	 */
	public void deleteFnBtnRel(String fnId);

	/**
	 * 批量删除功能所关联的按钮
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @param ids
	 */
	public void deleteFnBtnRels(List<String> ids);

}