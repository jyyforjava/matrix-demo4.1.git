package com.zkingsoft.common.groovy;

public interface GroovyRunable {

	public Object run();
}
