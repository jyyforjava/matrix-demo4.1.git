package com.zkingsoft.common.authority.tag;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.matrix.core.tools.StringUtils;

/**
 * tag字符串比较工具
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月9日
 */
public class AuthorityToolsTag extends SimpleTagSupport {

	private String id;
	private String setStr;
	StringWriter sw = new StringWriter();

	@Override
	public void doTag() throws JspException, IOException {

		if (StringUtils.isContentSet(id, setStr)) {
			getJspBody().invoke(sw);
			getJspContext().getOut().println(sw.toString());
		}

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSetStr() {
		return setStr;
	}

	public void setSetStr(String setStr) {
		this.setStr = setStr;
	}

}
