package com.zkingsoft.common.authority.tag;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.WebUtil;
import com.zkingsoft.common.authority.AuthorityManager;
import com.zkingsoft.common.authority.DefaultAuthorityManager;
import com.zkingsoft.common.constance.AppConstance;

/**
 * 按钮权限校验tag
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 10, 2017
 */
public class ButtonAuthorutyTag extends SimpleTagSupport {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	StringWriter sw = new StringWriter();

	@Override
	public void doTag() throws JspException, IOException {

	 AuthorityManager authority = (DefaultAuthorityManager) WebUtil.getBean(AppConstance.DEFAULT_AUTHORITYMANAGER);
		try {
			if (authority.isBtnPermitted(value)) {
				getJspBody().invoke(sw);
				getJspContext().getOut().println(sw.toString());
			}
		} catch (Exception e) {
			LogUtil.error("#按钮权限校验错误#value={}", e, value);
		}
	}

}