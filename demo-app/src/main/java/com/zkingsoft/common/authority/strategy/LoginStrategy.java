package com.zkingsoft.common.authority.strategy;

/**
 * 登录策略接口
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月9日
 */
public interface LoginStrategy {

	/**
	 * 登录接口，实现具体的登录验证方法，需要返回一个用户对象
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date Dec 10, 2017
	 * @return
	 */
	public Object login();

}
