package com.zkingsoft.common.authority.strategy;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.PropertiesUtil;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.zkingsoft.common.bean.SysUserLoginRecord;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.common.constance.AppConstance;
import com.zkingsoft.common.constance.AppMessageCode;
import com.zkingsoft.common.service.SysUsersService;
import com.zkingsoft.common.tools.PasswordUtil;

/**
 * 账号密码登录策略
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月9日
 */
public class AccountPasswordLogin implements LoginStrategy {

	private SysUsersService sysUsersService;

	private SysUsers user;

	public AccountPasswordLogin(SysUsers user, SysUsersService sysUsersService) {
		this.user = user;
		this.sysUsersService = sysUsersService;
	}

	@Override
	public Object login() {

		// 是否同时有账号和密码
		if (StringUtils.isBlank(user.getSuAccount()) || StringUtils.isBlank(user.getSuPassword())) {
			throw new GlobleException(AppMessageCode.User.ACCOUNT_PASSWORD_MUST_REQUEST);
		}
		// 根据账号查询用户
		SysUsers userQuery = new SysUsers();
		userQuery.setSuAccount(user.getSuAccount());
		List<SysUsers> users = sysUsersService.findByModel(userQuery);
		if (users.size() != 1) {
			LogUtil.error("{}账号不唯一，或不存在登录失败，userList={}", null, user.getSuAccount(), users);
			throw new GlobleException(AppMessageCode.User.ACCOUNT_NOT_EXIST);
		}

		userQuery = users.get(0);

		// 后台已经删除账号
		if (userQuery.getSuValid().equals(AppConstance.RECORD_INVALID)) {
			throw new GlobleException(AppMessageCode.User.ACCOUNT_IS_DELETED);
		}
		if (StringUtils.isBlank(userQuery.getSuAccountStatus())) {
			LogUtil.info("用户状态为空登录失败");
			throw new GlobleException(AppMessageCode.User.ACCOUNT_NOT_ACTIVE);
		}
		// 判断用户的状态
		switch (userQuery.getSuAccountStatus()) {
		case AppConstance.ACCOUNT_STATUS_INACTIVATED:
			throw new GlobleException(AppMessageCode.User.ACCOUNT_NOT_ACTIVE);
		case AppConstance.ACCOUNT_STATUS_LOCKED:
			throw new GlobleException(AppMessageCode.User.ACCOUNT_IS_LOCK);
		default:
			break;
		}
		// 用户密码比较
		try {
			user.setSuRegisterTime(userQuery.getSuRegisterTime());
			if (!userQuery.getSuPassword().equals(PasswordUtil.getEncrypUserPwd(user))) {
				accountOrPasswordError(user);
			}
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			LogUtil.error("密码加密方法出错", e, user.getSuAccount());
			throw new GlobleException(SystemErrorCode.SYSTEM_RUNNING_ERROR);
		}
		addErrorLoginRecord(userQuery, AppConstance.LOGIN_SUCCESS);
		// 登录成功
		return userQuery;
	}

	/**
	 * 根据安全策略处理账号或者密码错误
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月12日
	 * @param suAccount
	 */
	private void accountOrPasswordError(SysUsers loginUser) {
		String errorTimesStr = PropertiesUtil.getString(AppConstance.ERROR_PASSWORD_TIMES);

		if (StringUtils.isNotBlank(errorTimesStr) && !AppConstance.NOT_VALIDATE_ERROR_TIMES.equals(errorTimesStr)) {

			int sessionErrorTimes = sysUsersService.countUserTodayErrorLoginTimes(loginUser.getSuAccount());

			int errorTimes = Integer.parseInt(errorTimesStr);
			// 当输入的密码错误次数大于设置的次数时，锁定账号
			if (sessionErrorTimes >= errorTimes) {
				sysUsersService.lockUser(loginUser.getSuAccount());
				throw new GlobleException(AppMessageCode.User.ACCOUNT_IS_LOCK);
			} else {
				addErrorLoginRecord(loginUser, AppConstance.LOGIN_FAIL);
			}
			throw new GlobleException(AppMessageCode.User.ACCOUNT_PASSWORD_ERROR, errorTimesStr, ++sessionErrorTimes,
					errorTimesStr);
		}

	}

	/**
	 * 添加登录记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年12月12日
	 * @param loginUser
	 * @param loginResult
	 */
	private void addErrorLoginRecord(SysUsers loginUser, int loginResult) {

		if (AppConstance.LOGIN_SUCCESS.equals(loginResult)) {
			sysUsersService.cleanUserTodayErrorLoginTime(loginUser.getSuAccount());
		}
		SysUserLoginRecord loginRecord = new SysUserLoginRecord();
		loginRecord.setCreateBy(MatrixConstance.SYSTEM_USER);
		loginRecord.setUpdateBy(MatrixConstance.SYSTEM_USER);
		loginRecord.setLrLoginTime(new Date());
		loginRecord.setLrId(UUIDUtil.getRandomID());
		loginRecord.setUserAccount(loginUser.getSuAccount());
		loginRecord.setLrResult(loginResult);
		loginRecord.setLrIp(WebUtil.getCustomerIp());
		loginRecord.setLrValid(AppConstance.RECORD_VALID);
		sysUsersService.addUserLoginRecord(loginRecord);
	}

}
