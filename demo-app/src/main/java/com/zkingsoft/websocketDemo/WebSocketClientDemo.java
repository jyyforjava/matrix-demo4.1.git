package com.zkingsoft.websocketDemo;

import java.io.IOException;
import java.net.URI;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.matrix.core.tools.LogUtil;


/**
 * 初始化本地java客户端的类
 * @author zhangheng
 *
 */
public class WebSocketClientDemo {
     
	// 连接服务端的地址
	private static String uri = "ws://" + "localhost:8080"+ "/demo-app/websocket";
    private static Session session;
    
    /**
     * 初始化本地java客户端的方法
     */
    public void start() {
        WebSocketContainer container = null;
        try {
            container = ContainerProvider.getWebSocketContainer();
        } catch (Exception ex) {
        	LogUtil.error("java客户端初始化失败", ex);
        }
            URI r = URI.create(uri);
            try {
				session = container.connectToServer(ClientDemo.class, r);
			} catch (DeploymentException e) {
				LogUtil.error("java客户端初始化失败", e);
			} catch (IOException e) {
				LogUtil.error("java客户端初始化失败", e);
			}
       
    }
    
}
