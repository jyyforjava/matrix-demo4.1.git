package com.zkingsoft.websocketDemo;


import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import org.apache.commons.collections.CollectionUtils;

import com.matrix.core.tools.LogUtil;


/**
 * java客户端,用于接收服务端的消息并通过本地的服务端发送到web端
 * @author zhangheng
 *
 */
@ClientEndpoint
public class ClientDemo {
	
	@OnOpen
	public void onOpen(Session session) {
		LogUtil.info("本地java客户端已连接");
	}

	@OnMessage
	public void onMessage(String message) throws IOException {
		//客户端这里接收消息后，调用本地服务端客户端发送消息到web
		// 第一步 获得所有本地的连接
		Map<String,WebSocketServer2> connectionsMap = WebSocketServer2.connectionsMap;
		Collection<WebSocketServer2> connections = connectionsMap.values();
		if(CollectionUtils.isNotEmpty(connections)) {
			LogUtil.info("java本地客户端接收到的消息: " + message);
			// 给所有的连接客户端转发收到的消息
			for(WebSocketServer2 server :connections) {
				server.sendMessage(server, message);
			}
		}
	}

	@OnClose
	public void onClose() {
     
	}
}
