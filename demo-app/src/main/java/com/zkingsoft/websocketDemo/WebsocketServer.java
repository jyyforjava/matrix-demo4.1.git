package com.zkingsoft.websocketDemo;

import java.io.IOException;
import java.util.ArrayList;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * 模拟服务提供方的websocket的服务端 (例如中车的大数据平台)
 * 
 * @author zhangheng
 *
 */
// 该无服务端的websocket连接如下所示
// ws://localhost:8080/matrix-app/websocket
// 页面：http://localhost:8080/demo-app/websocketView/websocket.jsp
@ServerEndpoint("/websocket")
public class WebsocketServer {
	// 用于存放所有连接
	public static final ArrayList<WebsocketServer> connections = new ArrayList<WebsocketServer>();
	// 表示和客户端的一次会话
	private Session session;
	private String userName;
    
	/**
	 * 当有新的客户端连接会触发该方法
	 * @param session
	 */
	@OnOpen
	public void connect(Session session) {
		this.session = session;
		connections.add(this);
	}

	/**
	 * 接收客户端的消息
	 * @param message
	 * @throws IOException
	 */
	@OnMessage
	public void receiveMessage(String message) throws IOException {
		WebsocketServer server = new WebsocketServer();
		System.out.println("收到的消息：" + message);
		if (message.startsWith("webinit")) {
			this.userName = message.split("-")[1];
			sendMessage(this, "web建立连接");
			// 模拟crrc的消息
		} else if (message.startsWith("crrc")) {
			for (int i = 0; i < connections.size(); i++) {
				server = connections.get(i);
				sendMessage(server, message);
			}

		}
	}
    
	/**
	 * 用于发消息的公共方法
	 * @param server
	 * @param message
	 * @throws IOException
	 */
	public void sendMessage(WebsocketServer server, String message) throws IOException {
		server.session.getBasicRemote().sendText("from java:" + message);
	}
}
