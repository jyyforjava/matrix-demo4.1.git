package com.zkingsoft.websocketDemo;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import com.matrix.core.tools.LogUtil;

/**
 * 本地服务端
 * @author zhangheng
 *
 */
// 页面展示 http://localhost:8080/demo-app/websocketView/zking.jsp?username=test
@ServerEndpoint("/zkingsocket/{username}")
public class WebSocketServer2 {
	
	// 用于存储所有连接的map,username为键
	public static final Map<String,WebSocketServer2> connectionsMap = new ConcurrentHashMap<String,WebSocketServer2>();
	private Session session;
	private static WebSocketClientDemo client=null;
	private String username;
    
	/**
	 * 本地服务有新连接时的监听方法
	 * @param username
	 * @param session
	 */
	@OnOpen
	public void connect(@PathParam("username") String username,Session session) {
		if(null==client) {
			// 在这里初始化本地java客户端连接
			client = new WebSocketClientDemo();
			client.start();
		}
		this.username = username;
		this.session = session;
		// 将本次连接添加到map中
		connectionsMap.put(username, this);
	}
    
	@OnMessage
	public void receiveMessage(String message) throws IOException {
		LogUtil.info("本监听消息的方法");
	}
	
	@OnClose
	public void onClose() {
		connectionsMap.remove(username);
	}

	/**
	 * 本地服务端向客户端发送消息的公共方法
	 * @param server
	 * @param message
	 * @throws IOException
	 */
	public void sendMessage(WebSocketServer2 server, String message) throws IOException {
		server.session.getBasicRemote().sendText("From zking To "+server.username+" :" + message);
	}
}
