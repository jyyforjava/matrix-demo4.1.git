package com.zkingsoft.baiduEditor.bean;

import com.zkingsoft.common.bean.EntityDTOExt;

import java.util.Date;

import com.matrix.core.anotations.Extend;

/**
 * @description (新闻表)
 * @author 姜友瑶
 * @date 2018-06-20 14:18
 */
public class BizNews  extends EntityDTOExt{
	@Extend
	private static final long serialVersionUID = 1L; 

	
	/**
	 * 主键
	 */
	private String  newsId;
			
	
	/**
	 * 新闻标题
	 */
	private String  newsTitle;
			
	
	/**
	 * 新闻摘要
	 */
	private String  newsAbstract;
			
	
	/**
	 * 新闻内容
	 */
	private String  newsContent;
			
	
	/**
	 * 新闻图片
	 */
	private String  newsImage;
			
	
	/**
	 * 类型
	 */
	private Integer  newsType;
			
	
	/**
	 * 阅读次数
	 */
	private Integer  newsReadTotal;
			
	
	/**
	 * 作者
	 */
	private String  newsReleaseAuthor;
			
	
	/**
	 * 发布时间
	 */
	private Date  newsReleaseTime;
			
	
	/**
	 * 发布状态(1:未发布,2:已发布)
	 */
	private Integer  newsReleaseStatus;
			
	

	public String getNewsId() {
		return newsId;
	}
   	
   	public void setNewsId(String newsId) {
		this.newsId=newsId;
	}
   	

	public String getNewsTitle() {
		return newsTitle;
	}
   	
   	public void setNewsTitle(String newsTitle) {
		this.newsTitle=newsTitle;
	}
   	

	public String getNewsAbstract() {
		return newsAbstract;
	}
   	
   	public void setNewsAbstract(String newsAbstract) {
		this.newsAbstract=newsAbstract;
	}
   	

	public String getNewsContent() {
		return newsContent;
	}
   	
   	public void setNewsContent(String newsContent) {
		this.newsContent=newsContent;
	}
   	

	public String getNewsImage() {
		return newsImage;
	}
   	
   	public void setNewsImage(String newsImage) {
		this.newsImage=newsImage;
	}
   	

	public Integer getNewsType() {
		return newsType;
	}
   	
   	public void setNewsType(Integer newsType) {
		this.newsType=newsType;
	}
   	

	public Integer getNewsReadTotal() {
		return newsReadTotal;
	}
   	
   	public void setNewsReadTotal(Integer newsReadTotal) {
		this.newsReadTotal=newsReadTotal;
	}
   	

	public String getNewsReleaseAuthor() {
		return newsReleaseAuthor;
	}
   	
   	public void setNewsReleaseAuthor(String newsReleaseAuthor) {
		this.newsReleaseAuthor=newsReleaseAuthor;
	}
   	

	public Date getNewsReleaseTime() {
		return newsReleaseTime;
	}
   	
   	public void setNewsReleaseTime(Date newsReleaseTime) {
		this.newsReleaseTime=newsReleaseTime;
	}
   	

	public Integer getNewsReleaseStatus() {
		return newsReleaseStatus;
	}
   	
   	public void setNewsReleaseStatus(Integer newsReleaseStatus) {
		this.newsReleaseStatus=newsReleaseStatus;
	}
   	


  
}