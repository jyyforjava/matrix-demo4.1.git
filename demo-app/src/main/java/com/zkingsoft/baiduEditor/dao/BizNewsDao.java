package com.zkingsoft.baiduEditor.dao;

import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;
import com.matrix.core.pojo.PaginationVO;
import com.zkingsoft.baiduEditor.bean.BizNews;

/**
 * @description (新闻表)
 * @author 姜友瑶
 * @date 2018-06-20 14:18
 */
public interface BizNewsDao{

	public int insert(@Param("item") BizNews bizNews);
   	
   	public int batchInsert(@Param("list") List<BizNews> bizNewsList);
   	
	public int updateByMap(Map<String, Object> modifyMap);
	
	public int updateByModel(@Param("record")BizNews bizNews);
	
	public int deleteByIds(@Param("list") List<String> list);
	
	public int deleteById(String newsId);

	public int deleteByModel(@Param("record") BizNews bizNews);
	
	public List<BizNews> selectInPage(@Param("record") BizNews bizNews, @Param("pageVo") PaginationVO pageVo);

	public List<BizNews> selectByModel(@Param("record") BizNews bizNews);
	
	public int selectTotalRecord(@Param("record") BizNews bizNews);
	
	public BizNews  selectById(String newsId);
	
	public BizNews  selectForUpdate(String newsId);
	
	public int realseByIds(@Param("list") List<String> list,@Param("record")BizNews bizNews);
	
	public List<BizNews> findInPage(@Param("record") BizNews bizNews);
}