package com.zkingsoft.baiduEditor.action;

import static com.zkingsoft.common.constance.AppConstance.*;

import java.util.Date;
import java.util.List;

import com.matrix.core.constance.SystemMessageCode;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.web.BaseAction;
import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.anotations.RemoveRequestToken;
import com.zkingsoft.baiduEditor.service.BizNewsService;
import org.springframework.stereotype.Controller;
import com.matrix.core.exception.GlobleException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.matrix.core.anotations.SaveRequestToken;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.zkingsoft.common.constance.AppConstance;
import com.zkingsoft.baiduEditor.bean.BizNews;
import com.zkingsoft.baiduEditor.dao.BizNewsDao;

/**
 * @description (新闻表)
 * @author 姜友瑶
 * @date 2018-06-20 14:18
 */
@Controller
@RequestMapping(value = "admin/bizNews")
public class BizNewsAction extends BaseAction{

	@Autowired
	private BizNewsService bizNewsService;
	
	@Autowired
	private BizNewsDao bizNewsDao;
	//记录编辑前的值Before_Edit_Value
	public static final String BEV="BizNews_BEV";
	
	
	/**
	 * 列表显示
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(BizNews bizNews, PaginationVO pageVo) {
		bizNews.setNewsType(AppConstance.PARTY_NEWS);
		return showList(bizNewsService, bizNews, pageVo);
	}
   	
	/**
	 * 新增
	 */   
	@RemoveRequestToken	
   	@RequestMapping(value = "/addBizNews")
	public @ResponseBody AjaxResult addBizNews(BizNews bizNews) {
		// 新闻类型(1:党群新闻,2:工会新闻)
		bizNews.setNewsType(AppConstance.PARTY_NEWS);
		
	 	int i=bizNewsService.add(bizNews);
	 	if(i > 0){
	 		return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.ADD_SUCCES, "新闻表");
	 	}else {
			throw new GlobleException(SystemErrorCode.DATA_ADD_FAIL);
		}
	}
	
	
	
	
	
	/**
	 * 修改
	 */   
	@RemoveRequestToken	
   	@RequestMapping(value = "/modifyBizNews")
	public @ResponseBody AjaxResult modifyBizNews(BizNews bizNews) {
	   		
	   		AjaxResult result=modify(bizNewsService, WebUtil.getSessionAttribute(BEV), bizNews, "新闻表");
			WebUtil.removeSessionAttribute(BEV);
			return  result;
	}
	
	
	
	
   	/**
	 * 进入修改界面
	 */   
	@SaveRequestToken
   	@RequestMapping(value =  "/editForm")
	public String editForm(String id) {
		BizNews bizNews;
		if (id != null) {
			bizNews = bizNewsService.findById(id);
			WebUtil.getRequest().setAttribute("obj", bizNews);
			WebUtil.setSessionAttribute(BEV, bizNews);
		}
		return "admin/news/news-form";
	}
   	
   	
   	/**
	 * 删除
	 */  
 	@RequestMapping(value = "/del")
	public @ResponseBody AjaxResult del(String keys) {
		return remove(bizNewsService, keys);
	}
 	
 	/**
	 * 发布资讯
	 * 
	 * @date 2018年6月8日
	 */
	@RequestMapping(value =  "/release")
	public @ResponseBody AjaxResult releaseInformation(String keys) {
		// 将资讯id字符串修改成集合
		List<String> ids = StringUtils.strToCollToString(keys, ",");
		
		BizNews bizNews=new BizNews();
		bizNews.setNewsReleaseStatus(AppConstance.NEWS_STATUS_YR);
		int i=bizNewsDao.realseByIds(ids, bizNews);
		
		if (i > 0) {			
			return new AjaxResult(AjaxResult.STATUS_SUCCESS,"发布成功");
		} else {	
			return new AjaxResult(AjaxResult.STATUS_FAIL, "发布失败");
		}
	}
  
}