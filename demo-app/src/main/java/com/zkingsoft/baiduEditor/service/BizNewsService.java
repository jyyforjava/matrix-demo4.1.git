package com.zkingsoft.baiduEditor.service;

import java.util.List;
import com.matrix.core.pojo.PaginationVO;
import com.zkingsoft.baiduEditor.bean.BizNews;
import com.matrix.core.web.BaseServices;

/**
 * @description  service接口类 (新闻表)
 * @author 姜友瑶
 * @date 2018-06-20 14:18
 */
public interface BizNewsService  extends BaseServices<BizNews>{
	
	/**
	 * 新增
	 */
	public int add(BizNews bizNews);
   	
   	/**
	 * 批量新增
	 */
	public int batchAdd(List<BizNews>  bizNewsList);
   	
   	/**
	 * 根据map键值对 更新
	 */
	public int modifyByMap(BizNews oldBizNews ,BizNews newBizNews);
	
	/**
	 * 根据对象 更新
	 */
	public int modifyByModel(BizNews bizNews);
	
	/**
	 * 批量删除
	 */
	public int remove(List<String> list);

	/**
	 * 根据id删除
	 */
	public int removeById(String newsId);
	
	/**
	 * 根据对象删除
	 */
	public int removeByModel(BizNews bizNews);
	
	/**
	 * 分页查询
	 */
	public List<BizNews> findInPage(BizNews bizNews, PaginationVO pageVo);

	/**
	 * 根据对象查询
	 */
	public List<BizNews> findByModel(BizNews bizNews);
	
	/**
	 * 统计记录数
	 */
	public int  findTotal(BizNews bizNews);
	
	/**
	 * 根据id查询
	 */
	public BizNews  findById(String newsId);

   	

  
}