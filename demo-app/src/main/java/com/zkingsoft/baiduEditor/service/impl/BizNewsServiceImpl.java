package com.zkingsoft.baiduEditor.service.impl;

import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.constance.SystemErrorCode;
import com.zkingsoft.baiduEditor.service.BizNewsService;
import org.springframework.stereotype.Service;
import com.matrix.core.exception.GlobleException;
import com.zkingsoft.common.bean.SysUsers;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.tools.WebUtil;
import com.zkingsoft.baiduEditor.dao.BizNewsDao;
import org.springframework.beans.factory.annotation.Autowired;
import com.matrix.core.tools.ModelUtils;
import java.util.List;
import java.util.Map;
import com.zkingsoft.baiduEditor.bean.BizNews;

/**
 * @description service接口实现类(新闻表)
 * @author 姜友瑶
 * @date 2018-06-20 14:18
 */
@Service
public class  BizNewsServiceImpl implements  BizNewsService{

	
	@Autowired
	private BizNewsDao bizNewsDao;
	
	
	@Override
	public int add(BizNews bizNews){
		// 设置基本字段信息
		SysUsers loginUser = WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
		bizNews.setCreateBy(loginUser.getSuName());
		bizNews.setUpdateBy(loginUser.getSuName());
		bizNews.setNewsId(UUIDUtil.getRandomID());
		return bizNewsDao.insert(bizNews);
		
	}
	
	@Override
	public int batchAdd(List<BizNews>  bizNewsList) {
		//这里没有做基本字段的设置，如有需要请自己实现	
		int num = 0;
		int c = 10000;
		int size = bizNewsList.size()/c + 1;
		for(int i=0; i<size; i++) {
			int begin = i*c;
			int end = (i+1)*c;
			end = end >= bizNewsList.size() ? bizNewsList.size() : end;
			List<BizNews> insertList = bizNewsList.subList(begin, end);
			num += bizNewsDao.batchInsert(insertList);
		}
		return num;
		
	}
	
	
   	
    @Override
	public int modifyByMap(BizNews oldBizNews
	,BizNews newBizNews){
	
		Map<String, Object> modifyMap = null;
		try {
			if (!ModelUtils.isModified(oldBizNews, newBizNews)) {
				return MatrixConstance.DML_SUCCESSS;
			}
			modifyMap = ModelUtils.comparePojo2Map(oldBizNews, newBizNews);
		} catch (Exception e) {
			throw new GlobleException(SystemErrorCode.DATA_UPDATE_FAIL, e, newBizNews);
		}
		if (modifyMap.size() > 0) {
			modifyMap.put("newsId", oldBizNews.getNewsId());
			bizNewsDao.updateByMap(modifyMap);
		}
		return MatrixConstance.DML_SUCCESSS;
	}
	
	@Override
	public int modifyByModel(BizNews bizNews){
	
		return bizNewsDao.updateByModel(bizNews);
	
	}
	
	
	
	@Override
	public int remove(List<String> list){
	
		return bizNewsDao.deleteByIds(list);
	
	}

	@Override
	public int removeById(String newsId){
	
		return bizNewsDao.deleteById(newsId);
	
	}
	
	@Override
	public int removeByModel(BizNews bizNews){
	
		return bizNewsDao.deleteByModel(bizNews);
	
	}
	
	
	@Override
	public List<BizNews> findInPage(BizNews bizNews,  PaginationVO pageVo){
	
		return bizNewsDao.selectInPage(bizNews , pageVo);
	
	}
	
	@Override
	public List<BizNews> findByModel(BizNews bizNews){
	
		return bizNewsDao.selectByModel(bizNews);
	
	}
	
	@Override
	public int  findTotal(BizNews bizNews){
	
		return bizNewsDao.selectTotalRecord(bizNews);
	
	}
	
	@Override
	public BizNews  findById(String newsId){
	
		return bizNewsDao.selectById(newsId);
	
	}

   	
	
	
}