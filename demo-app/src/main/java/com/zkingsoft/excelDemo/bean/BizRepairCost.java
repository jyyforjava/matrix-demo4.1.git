package com.zkingsoft.excelDemo.bean;

import com.matrix.core.pojo.EntityDTO;
import com.zkingsoft.common.bean.EntityDTOExt;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.matrix.core.anotations.Extend;

/**
 * @description (维修保养费用)
 * @author 何明
 * @date 2018-06-19 15:12
 */
public class BizRepairCost  extends EntityDTOExt{
	@Extend
	private static final long serialVersionUID = 1L; 

	
	/**
	 * 主键
	 */
	private String  repairId;
			
	
	/**
	 * 维修厂
	 */
	private String  repairFactory;
			
	
	/**
	 * 维修时间
	 */
	private Date  repairTime;
			
	
	/**
	 * 车牌号
	 */
	private String  repairCarNum;
			
	
	/**
	 * 车辆id
	 */
	private String  carId;
			
	
	/**
	 * 维修费用
	 */
	private BigDecimal  repairCost;
			
	
	/**
	 * 经手人
	 */
	private String  repairHandlePerson;
			
	
	/**
	 * 项目及费用明细
	 */
	private String  repairItemDetail;
			
	
	/**
	 * 公里数
	 */
	private String  repairMileage;
			
	/**
	 * 开始时间
	 */
	@Extend
	private String  startTime;

	/**
	 * 结束时间
	 */
	@Extend
	private String  endTime;
	/**
	 * 
	 * 年月份
	 */
	@Extend
	private String monthYear;
	/**
	 * 录入人
	 */
	@Extend
	private String userName;
	
	
	
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public void setRepairTime(Date repairTime) {
		this.repairTime = repairTime;
	}

	public String getRepairId() {
			if(repairId==null){
			return "";
   		}
		
		return repairId;
	}
   	
   	public void setRepairId(String repairId) {
		this.repairId=repairId;
	}
   	

	public String getRepairFactory() {
			if(repairFactory==null){
			return "";
   		}
		
		return repairFactory;
	}
   	
   	public void setRepairFactory(String repairFactory) {
		this.repairFactory=repairFactory;
	}
   	

	public Date getRepairTime() {
		
		return repairTime;
	}
   	
   	public void setRepairTime(String repairTime) {
   		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH"); //加上时间
    	   Date date=null;
    	  
    	    try {
 			date = format.parse(repairTime);
 		} catch (java.text.ParseException e) {
 			e.printStackTrace();
 		}
    	     this.repairTime=date;
		//this.repairTime=repairTime;
	}
   	

	public String getRepairCarNum() {
			if(repairCarNum==null){
			return "";
   		}
		
		return repairCarNum;
	}
   	
   	public void setRepairCarNum(String repairCarNum) {
		this.repairCarNum=repairCarNum;
	}
   	

	public String getCarId() {
			if(carId==null){
			return "";
   		}
		
		return carId;
	}
   	
   	public void setCarId(String carId) {
		this.carId=carId;
	}
   	

	public BigDecimal getRepairCost() {
		
		return repairCost;
	}
   	
   	public void setRepairCost(BigDecimal repairCost) {
		this.repairCost=repairCost;
	}
   	

	public String getRepairHandlePerson() {
			if(repairHandlePerson==null){
			return "";
   		}
		
		return repairHandlePerson;
	}
   	
   	public void setRepairHandlePerson(String repairHandlePerson) {
		this.repairHandlePerson=repairHandlePerson;
	}
   	

	public String getRepairItemDetail() {
			if(repairItemDetail==null){
			return "";
   		}
		
		return repairItemDetail;
	}
   	
   	public void setRepairItemDetail(String repairItemDetail) {
		this.repairItemDetail=repairItemDetail;
	}
   	

	public String getRepairMileage() {
			if(repairMileage==null){
			return "";
   		}
		
		return repairMileage;
	}
   	
   	public void setRepairMileage(String repairMileage) {
		this.repairMileage=repairMileage;
	}
   	


  
}