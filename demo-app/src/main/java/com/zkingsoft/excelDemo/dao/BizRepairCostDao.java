package com.zkingsoft.excelDemo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.matrix.core.pojo.PaginationVO;
import com.zkingsoft.excelDemo.bean.BizRepairCost;


/**
 * @description (维修保养费用)
 * @author 何明
 * @date 2018-06-19 15:12
 */
public interface BizRepairCostDao{

	public int insert(@Param("item") BizRepairCost bizRepairCost);
   	
   	public int batchInsert(@Param("list") List<BizRepairCost> bizRepairCostList);
   	
	public int updateByMap(Map<String, Object> modifyMap);
	
	public int updateByModel(@Param("record")BizRepairCost bizRepairCost);
	
	public int deleteByIds(@Param("list") List<String> list);
	
	public int deleteById(String repairId);

	public int deleteByModel(@Param("record") BizRepairCost bizRepairCost);
	
	public List<BizRepairCost> selectInPage(@Param("record") BizRepairCost bizRepairCost, @Param("pageVo") PaginationVO pageVo);

	public List<BizRepairCost> selectByModel(@Param("record") BizRepairCost bizRepairCost);
	
	public int selectTotalRecord(@Param("record") BizRepairCost bizRepairCost);
	
	public BizRepairCost  selectById(String repairId);
	
	public BizRepairCost  selectForUpdate(String repairId);
	
	public List<BizRepairCost> selectRepairCount(@Param("record") BizRepairCost bizRepairCost);
	
	public int selectCountTotal(@Param("record") BizRepairCost bizRepairCost);
	
}