package com.zkingsoft.excelDemo.action;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.UUIDUtil;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.tools.excl.ExcelSheetPO;
import com.matrix.core.tools.excl.ExcelUtil;
import com.matrix.core.web.BaseAction;
import com.zkingsoft.common.bean.SysUsers;
import com.zkingsoft.excelDemo.bean.BizRepairCost;
import com.zkingsoft.excelDemo.dao.BizRepairCostDao;

/**
 * @description (维修保养费用)
 * @author 何明
 * @date 2018-06-19 15:12
 */
@Controller
@RequestMapping(value = "admin/bizRepairCost")
public class BizRepairCostAction extends BaseAction{

	
	//记录编辑前的值Before_Edit_Value
	public static final String BEV="BizRepairCost_BEV";
	@Autowired
	private BizRepairCostDao bizRepairCostDao;
	
	/**
	 * 查询车辆维修登记
	 */
	@RequestMapping(value =  "/selectRepairCount")
	public @ResponseBody AjaxResult selectRepairCount( BizRepairCost bizRepairCost,PaginationVO pageVo) {
		
		AjaxResult ajax=new AjaxResult();
		ajax.setStatus(AjaxResult.STATUS_SUCCESS);
		ajax.setRows(bizRepairCostDao.selectRepairCount(bizRepairCost));
		ajax.setTotal(bizRepairCostDao.selectCountTotal(bizRepairCost));
		return ajax;
		
	}
	
	/**
	 * 导出Excel文件
	 * 
	 * @author HEMING
	 * @email 910000889@qq.com
	 * @date 2018年07月18日
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/exportData")  
	public void exportData(ModelMap model,HttpServletRequest request,
			HttpServletResponse response,BizRepairCost bizRepairCost) throws IOException { 
	     //设置filename：导出文件名
        response.setHeader("Content-disposition", 
        		"filename="+new String("维修保养费用登记.xlsx".getBytes(), 
        				"ISO-8859-1"));         
		//查询数据
		List<BizRepairCost> costlist=bizRepairCostDao.selectRepairCount(bizRepairCost);
		SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
		
		//组合数据
        List<List<Object>> dataList = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(costlist)){
		      for(int i=0;i<costlist.size();i++){
		    	  BizRepairCost cost = costlist.get(i);
		    	  List<Object> temp = new ArrayList<>();
		    	 
		    	  String repairTime="";
		    	  if(cost.getRepairTime()!=null){
		    		  //格式化时间
		    		  repairTime=sdf.format(cost.getRepairTime());
		    	  }
		    	  temp.add(repairTime);
		    	  temp.add(cost.getRepairCarNum());
		    	  temp.add(cost.getRepairFactory());
		    	  temp.add(cost.getRepairCost());
		    	  temp.add(cost.getRepairHandlePerson());
		    	  temp.add(cost.getRepairItemDetail());
		    	  temp.add(cost.getRepairMileage());
		    	  dataList.add(temp);
		      }
       }
        
		ExcelSheetPO po=new ExcelSheetPO();
		//设置工作薄名称
		po.setSheetName("导出的报表");
		//设置标题
		po.setTitle("维修保养费用登记");
		//设置表格头
		String[] headers ={"时间","车号","维修厂","金额(元)","经手人","维修项目及配件单价","公里数"};
		po.setHeaders(headers);
		//dataList：需要导出的数据
		po.setDataList(dataList);
		List<ExcelSheetPO> poList=new ArrayList<ExcelSheetPO>();
		poList.add(po);
		OutputStream out = response.getOutputStream();    
		try {
			//导出数据
			ExcelUtil.createWorkbookAtOutStream(com.matrix.core.tools.excl.ExcelVersion.V2007,poList,out,true);
		} catch (IOException e) {
			LogUtil.error("导出报表异常：{}", e, "");
		}
		
	}
	
	/**
	 * 导入数据
	 * 
	 * @author HEMING
	 * @email 910000889@qq.com
	 * @date 2018年3月26日
	 * @param MultipartFile
	 *            file
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@RequestMapping(value = "/importData")
	public @ResponseBody AjaxResult importData(HttpServletResponse response, HttpServletRequest request,
			@RequestParam(value = "file", required = false) MultipartFile file) throws FileNotFoundException, IOException {
		//获取文件名
		String fileName = file.getOriginalFilename();
		//获取文件存放路径
		String dirPath = request.getSession().getServletContext().getRealPath("/") + "fileupload";
		
		File fileDir = new File(dirPath);
		// 检查目录
		if (!fileDir.exists()) {
			if (!fileDir.mkdirs()) {
				throw new GlobleException("创建文件失败");
			}
		}
		File saveFile = new File(dirPath + "/" + fileName);
		//把内存文件写入磁盘中
		file.transferTo(saveFile);
		//读取Excel中的数据
		List<ExcelSheetPO> data=ExcelUtil.readExcel(saveFile, null, null);
		LogUtil.debug("#读取的文件数据#{}", data.get(0).getDataList());
		//将读取的数据存入数据库
		List<BizRepairCost> costList=saveData(data.get(0).getDataList());
		if(CollectionUtils.isNotEmpty(costList)){
			bizRepairCostDao.batchInsert(costList);
			return  new AjaxResult(AjaxResult.STATUS_SUCCESS,costList);
		}
		return  new AjaxResult(AjaxResult.STATUS_FAIL,costList);

	}

	/**
	 * 将读取的数据存入数据库
	 */
	private List<BizRepairCost> saveData(List<List<Object>> excelData){
		List<BizRepairCost> data=new ArrayList<BizRepairCost>();
		if (CollectionUtils.isNotEmpty(excelData)) {
			for (int i = 1; i < excelData.size(); i++) {
				List<Object> objList = excelData.get(i);
				if (CollectionUtils.isNotEmpty(objList)) {
					BizRepairCost cost=new BizRepairCost();
					LogUtil.debug("#读取的数据#{}",objList.get(0).toString());
					SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
					Date time=new Date();
					try {
						time=sdf.parse(objList.get(0).toString());
					} catch (ParseException e) {
						LogUtil.error("#Excel导入#时间格式化异常：{}", e, "");
					}
					
					cost.setRepairTime(time);
					cost.setRepairCarNum(objList.get(1).toString());
					cost.setRepairFactory(objList.get(2).toString());
					cost.setRepairCost(new BigDecimal(objList.get(3).toString()));
					cost.setRepairHandlePerson(objList.get(4).toString());
					cost.setRepairItemDetail(objList.get(5).toString());
					cost.setRepairMileage(objList.get(6).toString());
					cost.setCreateBy(objList.get(4).toString());
					cost.setUpdateBy(objList.get(4).toString());
					cost.setRepairId(UUIDUtil.getRandomID());
					data.add(cost);
				}
			}
		}
		
		return data;
	}
}