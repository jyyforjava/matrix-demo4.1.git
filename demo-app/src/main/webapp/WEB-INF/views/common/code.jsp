<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE>
<html>
  <head>
    <title>${mycode.name }</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
  </head>
  <body style="max-width: 1000; text-align: center;">
    	<h1>${mycode.name }</h1>
    	<img alt="" style="max-width:100%"  src="${currentchildCode.qrcode }">
  </body>
</html>
