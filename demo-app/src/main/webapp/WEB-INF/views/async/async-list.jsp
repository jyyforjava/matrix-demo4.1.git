<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 搜索框部分start -->
		<div class="row">
			<div class="col-sm-12">
				<form class="form-inline" id="serchform">
					<div class="form-group mr-20">
						<select class="form-control " name="table">
							<option value="async_task">执行中任务</option>
							<option value="async_task_error">失败任务</option>
							<option value="async_task_success">成功任务-</option>
						</select>
					</div>
					<div class="form-group mr-20">
						<input id="search-text" name="taskType" placeholder="输入异步任务类型"
							type="text" class="form-control">
					</div>
					<div class="form-group mr-20">
						<label class="col-sm-2 control-label">创建日期</label> <input
							name="beginTime" type="text" placeholder="开始时间"
							class="form-control datetimepicker" id="beginTime"> <input
							placeholder="结束时间" name="endTime" type="text"
							class="form-control datetimepicker" id="endTime">
					</div>
					<div class="form-group mr-20">
						<button onclick="myGrid.serchData()" type="button"
							class="btn btn-info">
							<i class="fa fa-search "></i> 搜索
						</button>
						<button type="reset" class="btn btn-info ">
							<i class="fa fa-refresh "></i> 重置
						</button>
					</div>
				</form>
			</div>
		</div>
		<!-- 搜索框部分en -->
		<div class="ibox-content radius-5 mt-5 mpanel">

			<div class="row">
				<div class="col-sm-12">
					<div class="option-bar"></div>
					<table id="mgrid">
						<thead>
							<tr>
								<th data-checkbox="true"></th>
								<th data-formatter="MGrid.indexfn" data-align="center"
									data-width="30px">序号</th>
								<th data-align="center" data-width="150px" data-field="id"
									data-formatter="buidOperate">操作</th>
								<th data-field="id" data-sortable="true" width="200px">任务主键</th>
								<th data-field="pkgId" data-sortable="true">异步包主键</th>
								<th data-field="batchNo" data-sortable="true">批次号</th>
								<th data-field="taskType" data-sortable="true">任务类型</th>
								<th data-field="status" data-formatter="status">状态</th>
								<th data-field="retryTimes">重试次数</th>
								<th data-field="processedBy">处理任务的主机名称</th>
								<th data-field="createTime" data-formatter="MGrid.getTime">创建时间</th>
								<th data-field="createBy">创建人</th>
								<th data-field="initTime" data-formatter="MGrid.getTime">
									任务初始化时间</th>
								<th data-field="enqueueTime" data-formatter="MGrid.getTime">入队时间</th>
								<th data-field="dequeueTime" data-formatter="MGrid.getTime">
									出队时间</th>
								<th data-field="startTime" data-formatter="MGrid.getTime">开始时间</th>
								<th data-field="finishTime" data-formatter="MGrid.getTime">
									完成时间</th>

							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
		function status(value, row, index) {
			if (value == "0") {
				return "待处理";
			} else if (value == "1") {
				return "处理中";
			} else if (value == "2") {
				return "成功";
			} else if (value == "3") {
				return "失败";
			} else if (value == "4") {
				return "异常";
			}
		}
		var myGrid;

		$(function() {
			myGrid = MGrid.initGrid({
				url : "${path}/do/admin/async/showList",
				detailView : true,
				onExpandRow : function(index, row, $detail) {
					var cur = $detail.html('<div class=" box"></div>');
					var html = [];
					var i = 0;
					for ( var obj in row.taskData) {
						html[i] = '<p>' + obj + ' : ' + row.taskData[obj]
								+ '</p>'
						i++;
					}
					cur.append(html.join(""));
				},
			});

			var _initParam = {
				format : 'yyyy-mm-dd hh:ii',
				todayBtn : true,
				autoclose : true,
				startView : 2,
				maxView : 3,
				minView : 0
			};
			MTools.ininDatetimepicker(_initParam);
			//限制结束时间不小于开始时间
			var initParam = {
				format : 'yyyy-mm-dd hh:ii',
				todayBtn : true,
				autoclose : true,
				startView : 2,
				maxView : 3,
				minView : 0
			};
			MTools.limitStartEndTime(initParam);

		});

		function buidOperate(value, row, index) {
			var html = [];
			html[0] = '<a onClick="copy(\'' + value
					+ '\')"  title="复制" class="fa fa-copy option"></a>'

			html[1] = '<a onClick="del(\'' + value + '\',\'' + row.pkgId
					+ '\')"  title="删除" class="fa fa-trash option"></a>'
			return html.join("");
		}
		function copy(id) {
			MTools.handleItem('${path}/do/admin/async/copy/' + id, "确认要复制吗？",
					function() {
						myGrid.serchData();
					});
		}
		function del(id, pkgId) {
			MTools.handleItem('${path}/do/admin/async/del/' + id + '/' + pkgId,
					"确认删除吗？", function() {
						myGrid.serchData();
					});
		}
	</script>
</body>
</html>
