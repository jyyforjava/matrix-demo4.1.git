<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
		
	<style type="text/css">

	</style>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 搜索框部分start -->
		<matrix:btn value="car:search">
			<div class="row">
				<div class="col-sm-12">
					<form class="form-horizontal" id="dataform"
						onsubmit="javascripr:return false;">
							<div class="form-group">
								<label class="col-sm-2 control-label">上传文件<span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<input onclick="importData()"
										class="input-text upload-url radius form-control" type="text" id="showFileName"
										readonly style="width: 30%;display: inline;"> 
										<a onclick="importData()" class="btn btn-primary radius"> 
										<i class="fa fa-cloud-upload"></i> 浏览文件 </a> 
										<a class='btn btn-primary radius' download href="<c:out value='${path}/resource/excel/template.xlsx'></c:out>">下载模板</a> 
									<button type="button"onclick="submitData()" class="btn btn-success radius">导入</button>&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="file" name="file" accept=".xls,.xlsx" type="hidden" id="chanceUploadId" style="display:none">
									
								</div>
					  
							</div>
					</form>
				</div>
			</div>
		</matrix:btn>
		<!-- 搜索框部分 -->
		<div class="ibox-content radius-5 mt-5 mpanel">
			<div class="row">
				<div class="col-sm-12">
					<table id="mgrid">
						<thead>
							<tr>
								<th data-formatter="MGrid.indexfn" data-align="center"
									data-width="30px">序号</th>
								<th data-field="repairTime"  data-formatter="MGrid.getTime" >维修时间</th>
								<th data-field="repairCarNum" >车牌号</th>
								<th data-field="repairFactory"  >维修厂</th>
								<th data-field="repairCost">金额(元)</th>
								<th data-field="repairHandlePerson" >经手人</th>
								<th data-field="repairItemDetail">项目及费用明细</th>
								<th data-field="repairMileage">公里数</th>
							</tr>
						</thead>
						</table>
					
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
<script type="text/javascript" src="${path }/resource/js/plugin/ajaxfileupload.js">  </script>
	<script type="text/javascript">

		var myGrid;
		MTools.autoFullSelect();
		$(".select2").select2();
	 	$(function() {
			myGrid = MGrid.initGrid({
				url : "${path}/do/admin/bizRepairCost/selectRepairCount",
			});
			
		}); 

		//绑定文件的点击事件
		function importData(){
			var file=$("#chanceUploadId").click();
		}
		//选中文件，文件名字显示在文本框中
		$("#chanceUploadId").change(function(){
			$("#showFileName").val('');
			$("#showFileName").val($(this).val());
		})
		
		//提交文件
		function submitData(){
			if($("#showFileName").val()=='' || $("#showFileName").val().length<1){
				layer.msg("请选择上传文件！",{icon:2});
				return ;
			}
			var fileName = $("#showFileName").val();
			fileName = fileName.substring(fileName.lastIndexOf("\\")+1,fileName.lastIndexOf("."));
		
			 var index = layer.load(1, {
	        	  shade: [0.6,'#fff'] //0.1透明度的白色背景
	        	});
			 
			$.ajaxFileUpload({
				url : "${path}/do/admin/bizRepairCost/importData",
				type : "POST",
				secureuri : false,// 一般设置为false
				fileElementId : "chanceUploadId",// 文件上传空间的id属性 <input type="file" id="uploadId" />
				dataType : "json",
				success : function(data) {
					
					if(data.status=='200'){
						alert("导入成功");
						location.reload() 
					}else{
						alert("导入失败");
					}
					layer.close(index);
				}
			}); 
		}
		
		
	</script>
</body>
</html>
