<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
		
	<style type="text/css">

	</style>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 搜索框部分start -->
		<matrix:btn value="car:search">
			<div class="row">
				<div class="col-sm-12">
					<form class="form-inline" id="serchform">
							
							<div class="form-group mr-20">
								<label>车牌号：</label>
								<input id="search-text" name="repairCarNum" placeholder="输入车牌号"
									type="text" class="form-control">
							</div> 
							<div class="form-group mr-20">
								<label>经手人：</label>
								<input id="search-text" name="repairHandlePerson" placeholder="输入经手人"
									type="text" class="form-control">
							</div> 
							<div class="form-group mr-20">
								<label>开始时间：</label>
								<input  readOnly type="text" class="datetimepicker" style="width:150px;" 
								id="startTime" name="startTime" >
							</div> 
							<div class="form-group mr-20">
								<label>结束时间：</label>
								<input  readOnly type="text" class="datetimepicker" style="width:150px;" 
								id="endTime" name="endTime" >
							</div> 
							<div class="form-group mr-20">
								<button onclick="myGrid.serchData()" type="button"
									class="btn btn-info">
									<i class="fa fa-search "></i> 搜索
								</button>
								<button type="reset" class="btn btn-info ">
									<i class="fa fa-refresh "></i> 重置
								</button>
							</div>
						
					</form>
				</div>
			</div>
		</matrix:btn>
		<!-- 搜索框部分 -->
		<div class="ibox-content radius-5 mt-5 mpanel">
			<div class="row">
				<div class="col-sm-12">
					 <div class="option-bar">
						
								
							<button onclick="getDown()" type="button"
								class="btn btn-primary btn-sm">导出</button>
						 
					</div> 
 
				
					<table id="mgrid">
						<thead>
							<tr>
								<!-- <th data-checkbox="true"></th> -->
								<th data-formatter="MGrid.indexfn" data-align="center"
									data-width="30px">序号</th>
								<th data-field="repairTime"  data-formatter="MGrid.getTime" >维修时间</th>
								<th data-field="repairCarNum" >车牌号</th>
								<th data-field="repairFactory"  >维修厂</th>
								<th data-field="repairCost">金额(元)</th>
								<th data-field="repairHandlePerson" >经手人</th>
								<th data-field="repairItemDetail">项目及费用明细</th>
								<th data-field="repairMileage">公里数</th>
								<!-- <th data-align="center"  data-width="150px" data-field="carId" data-formatter="buidOperate">操作</th> -->
							</tr>
						</thead>
						</table>
					
				</div>
			</div>
		</div>
	</div>
	<div id="print" style="display:none" >
		<table width="100%" id="printTable">
		</table>
	</div>
	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
	
	<script type="text/javascript">

		var myGrid;
		MTools.autoFullSelect();
		$(".select2").select2();
		$(function() {
			myGrid = MGrid.initGrid({
				url : "${path}/do/admin/bizRepairCost/selectRepairCount",
				onLoadSuccess:function(data){
					
				},
			});
			//时间
			MTools.ininDatetimepicker({
				format : "yyyy-mm-dd", //默认显示年与日，如果想显示十分秒:"yyyy-mm-dd hh:ii:ss"
				minView: "month", 	//"month",只显示年月日的选择,不会再跳转去选择时分秒；如果想要选择时分秒的:"hour"
			});
			MTools.limitStartEndTime({
				beginTimeId :"#startTime",
				endTimeId : "#endTime"
			})
		});

		
		function buidOperate(value, row, index) {
			var html = [];
			<matrix:btn value="car:editForm">
			html[0] = '<a onClick="modifyTemplate(\'' + value
					+ '\')"  title="编辑" class="fa fa-edit option"></a>'

					html[1] = '<a onClick="delItem(\'' + value
							+ '\')" class="fa fa-trash option text-danger"></a>'
			</matrix:btn>
			return html.join("");
				
		}
	
		/** 获取时间 */
		 function getTimeDD(value, row, index) {
			var temp = "";
			try {
				temp = value
				var date = new Date(temp);
				if (!temp) {
					return "";
				} else {
					return date.getFullYear() + "-" + setFomartZero((date.getMonth() + 1))
					+ "-" + setFomartZero(date.getDate()) ;
				}
			} catch (e) {
				console.err('MGrid getTime执行失败');
			}
			return temp;
		}
		
		 function getDown(){
				window.location.href="${path}/do/admin/bizRepairCost/exportData";
				
			}
	</script>
</body>
</html>
