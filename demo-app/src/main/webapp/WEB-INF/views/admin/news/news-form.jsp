﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="../images/favicon.ico">
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>

<script type="text/javascript" charset="utf-8"
	src="${path }/resource/plugin/beditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"
	src="${path }/resource/plugin/beditor/ueditor.all.js">
	
</script>
<script type="text/javascript" charset="utf-8"
	src="${path }/resource/plugin/beditor/lang/zh-cn/zh-cn.js"></script>

<!-- 文件上传需要的样式 -->
<%-- <link rel="stylesheet" type="text/css"
	href="${path }/resource/plugin/kindeditor/themes/default/default.css" />
<link rel="stylesheet" type="text/css"
	href="${path }/resource/plugin/kindeditor/plugins/code/prettify.css" />
<script type="text/javascript"
	src="${path }/resource/plugin/kindeditor/kindeditor.js"></script>
<script type="text/javascript"
	src="${path }/resource/plugin/kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript"
	src="${path }/resource/plugin/kindeditor/plugins/code/prettify.js"></script> --%>
</head>

<div class="ibox-content">
	<form class="form-horizontal" id="dataform"
		onsubmit="javascripr:return false;">
		<input type="hidden" name="tokenUrl" value="${tokenUrl}">	 
		<input type="hidden" name="token" value="${token}">
		<c:if test="${obj ne null }">
			<input type="hidden" name="newsId" id="newsId" value="${obj.newsId}">
		</c:if>
		
		<div class="form-group">
			<label class="col-sm-2 control-label">标题<span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="newsTitle" maxlength="50" 
					value="<c:out value="${obj.newsTitle}" />" datatype="*1-100" nullmsg="标题不能为空">
			</div>
			
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">作者名称<span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="newsReleaseAuthor" maxlength="50" 
					value="<c:out value="${obj.newsReleaseAuthor}" /> " datatype="*1-50" nullmsg="作者名称不能为空">
			</div>
			
		</div>
		
		<div class="form-group">
				<label class="col-sm-2 control-label">是否发布<span
					class="text-danger">*</span></label>
				<div class="col-sm-4">
					<label class="radio-inline"><input type="radio"
						name="newsReleaseStatus" checked value="2"> 是</label> <label
						class="radio-inline"><input type="radio" name="newsReleaseStatus" checked 
						value="1"> 否</label>
				</div>
			</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">文章摘要</label>
			<div class="col-sm-8">
				<input value="<c:out value="${obj.newsAbstract}" />" datatype="*1-64"
					ignore="ignore" name="newsAbstract" class="form-control"
					type="text" id="digest" placeholder="自动抓取为文章前面64个字符" >
					
				<input class='btn radius btn-primary' type="button" value="自动抓取" 
					onclick="autoZy()"> 
					<input class='btn radius btn-primary'
					type="button" value="清空摘要" onclick="cleanZy()">
			</div>
			
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">文章缩略图</label>
			
			<div class="col-sm-8">
				<input  id="newsImage" name="newsImage" class="input-text upload-url radius form-control inpimg" placeholder="缩略图必须小于64KB,仅支持jpg/png格式，大小必须在1MB以下"
						type="text" value="${obj.newsImage }" readonly style="width:600px;display: inline;" dataType="*" nullmsg="请上传图片"/> 
						<a class="btn btn-primary radius upload-a" style="margin-left:10px;">
						<i class="fa fa-cloud-upload"></i> 上传图片</a>
			</div>
			
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label">文章内容</label>
			
			<div class="col-sm-8">
					<script style="width: 100%; height: 500px" id="container" 
						name="newsContent" type="text/plain">
						${obj.newsContent}
				 </script>
				</div>
		</div>
		<div class="form-group ">
			<div class="col-sm-12 text-center">
				<a href="javascript:;" onclick="myForm.submit()"
					class="btn btn-success radius"><i class="fa fa-check"></i>保存</a>&nbsp;&nbsp;&nbsp;&nbsp; 
				<button type="button" class="btn btn-danger radius" onclick="MTools.closeForm()"><i class="fa fa-close"></i>取消</button>
			</div>
		</div>
	</form>

</div>

</body>
	<script type="text/javascript"
	src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
		//var editor = null;
		var invokeUrl = "${path}/do/admin/bizNews/addBizNews";
		<c:if test="${obj ne null }">
		invokeUrl = "${path}/do/admin/bizNews/modifyBizNews";
		</c:if>
		/* 需要修改的配置：
		1：/resource/plugin/beditor/jsp/config.json
		需要修改下面参数对应的路径：(图片访问路径前缀 )
		imageUrlPrefix
		scrawlUrlPrefix
		snapscreenUrlPrefix
		catcherUrlPrefix
		videoUrlPrefix
		fileUrlPrefix
		imageManagerListPath
		fileManagerListPath */
		var editor = null;
		editor = UE.getEditor('container');
		var myForm=MForm.initForm({
			invokeUrl:invokeUrl,
			afterSubmit:function(){
				parent.myGrid.serchData();
			},
		});
		
		$(function() {	
			MUI.initImgUpload(".inpimg");
		 $(".select2").select2({'width':'100%'});
			
		});
		
		function autoZy() {
			var text =editor.getContent();
			$("#digest").val(text.replace(/<[^>]+>/g,"").substring(0, 64));
			
		}
		function cleanZy() {
			$("#digest").val("");
		}
		//上传文件回调
		function upcallback(inputId, url) {
			$('#' + inputId).val(url);
			$('#img_1').attr('src', url);
			//layer.closeAll('loading');
		}
	</script>
</html>