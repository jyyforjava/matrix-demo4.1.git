<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
	<style type="text/css">
	.select2-container--open .select2-dropdown--below{
	    z-index: 198910141;
	}
	</style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
	<!-- 搜索框部分start -->
 <matrix:btn value="goods:search">
	<div class="row" >
		<div class="col-sm-12" >
			<form class="form-inline" id="serchform">
				<div class="input-group" >
				
				</div>
			</form>
		</div>
	</div>
</matrix:btn>
	<!-- 搜索框部分en -->
<div class="ibox-content radius-5 mt-5 mpanel">
		
	<div class="row" >
		<div class="col-sm-12" >
		<div class="option-bar" >
			<matrix:btn value="article:del">
				<button  onclick="myGrid.delItems('newsId')" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash" ></i>批量删除</button>
			</matrix:btn>
							
			<matrix:btn value="article:add">
				<button onclick="openAdd()" type="button" class="btn btn-success btn-sm"><i class="fa fa-plus" ></i>新增</button>
			</matrix:btn>
			
			</div>
			<table id="mgrid" >
				<thead>
					<tr>
						<th data-checkbox="true"></th>
						<th data-formatter="MGrid.indexfn" data-align="center"  data-width="30px" >序号</th>
						<th data-field="newsTitle" >标题</th>
						<th data-field="newsReleaseAuthor" >作者</th>
						<th data-field="newsAbstract" >摘要</th>
						<th data-field="newsImage" data-formatter="MGrid.getImage">缩略图</th>
						<th data-field="newsReleaseStatus" data-formatter="isSend">是否发布</th>
						<th data-align="center"  data-width="150px" data-field="newsId" data-formatter="buidOperate">操作</th>
					</tr>
				</thead>
			</table>
	</div>
	</div>
</div>
</div>

<script type="text/javascript"
src="${path }/resource/js/systools/MJsBase.js"></script>
<script type="text/javascript">
	
	var myGrid;
	var ids;
	var items;
		$(function(){
			var delPath="";
			<matrix:btn value="article:del">
			delPath="${path}/do/admin/bizNews/su/del";
			</matrix:btn>
			 myGrid=MGrid.initGrid({
				 url:"${path}/do/admin/bizNews/showList",
				 delUrl:delPath
			 });
			 MTools.autoFullSelect();
				$(".select2").select2();
			 
			 
		});
		
		function isSend(val){
			if(val=='1'){
				return "未发布"
			}else{
				return "发布"
			}
		}
		function buidOperate(value, row, index){
			var html=[];
			<matrix:btn value="article:edit">
			html[0]='<a onClick="openEdit(\''+value+'\')"  title="编辑" class="fa fa-edit option"></a>'
			</matrix:btn>		
			return html.join(""); 
		}
		<matrix:btn value="article:add">
		//打开添加界面
		function openAdd() {
			layer.full(layer.open({
				type : 2,
				title : "添加新闻",
				maxmin : true,
				area : [ MUI.SIZE_L, '500px' ],
				content : [ '${path}/do/admin/bizNews/editForm']
			})); 
		}
		</matrix:btn>
		<matrix:btn value="article:edit">
		//打开编辑界面
		function openEdit(id) {
			layer.full(layer.open({
				type : 2,
				title : "编辑新闻",
				area : [ MUI.SIZE_L, '500px' ],
				maxmin : true,
				content : [ '${path}/do/admin/bizNews/editForm?id=' + id]
			}));
		}
		</matrix:btn>
	
		
		
		function releaseItems(newsId) {
			var keys = selectItemId(newsId);
			var url='${path}/do/admin/bizNews/release';
			MTools.handleItem(url + "?keys="
					+ keys, "确认要发布吗？", function() {
				myGrid.serchData();
			});
		}
		// 获取页面的id
		function selectItemId(idFiledName) {
			if (!idFiledName) {
				idFiledName = "id";
			}
			var items = $("#mgrid").bootstrapTable('getSelections');
			var keys = "";
			for (var i = 0; i < items.length; i++) {
				if (i == items.length - 1) {
					keys += items[i][idFiledName];
				} else {
					keys += items[i][idFiledName] + ",";
				}
			}
			return keys;
		};
	
		
		
	</script>
</body>
</html>
