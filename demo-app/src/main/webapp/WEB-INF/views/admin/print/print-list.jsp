<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
		
	<style type="text/css">

	</style>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 搜索框部分start -->
		<matrix:btn value="car:search">
			<div class="row">
				<div class="col-sm-12">
					<form class="form-inline" id="serchform">
						
						
					</form>
				</div>
			</div>
		</matrix:btn>
		<!-- 搜索框部分 -->
		<div class="ibox-content radius-5 mt-5 mpanel">
			<div class="row">
				<div class="col-sm-12">
					 <div class="option-bar">
						
							<button onclick="print()" type="button"
								class="btn btn-success btn-sm">打印</button>
						 
					</div> 
 
				
					<table id="mgrid">
						<thead>
							<tr>
								<!-- <th data-checkbox="true"></th> -->
								<th data-formatter="MGrid.indexfn" data-align="center"
									data-width="30px">序号</th>
								<th data-field="repairTime"  data-formatter="MGrid.getTime" >维修时间</th>
								<th data-field="repairCarNum" >车牌号</th>
								<th data-field="repairFactory"  >维修厂</th>
								<th data-field="repairCost">金额(元)</th>
								<th data-field="repairHandlePerson" >经手人</th>
								<th data-field="repairItemDetail">项目及费用明细</th>
								<th data-field="repairMileage">公里数</th>
								<!-- <th data-align="center"  data-width="150px" data-field="carId" data-formatter="buidOperate">操作</th> -->
							</tr>
						</thead>
						</table>
					
				</div>
			</div>
		</div>
	</div>
	<div id="print" style="display:none" >
		<table width="100%" id="printTable">
		</table>
	</div>
	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
		<script type="text/javascript" src="${path }/resource/js/plugin/LodopFuncs.js"></script>
		<script type="template" id="initTr">
		<tr>
								<th >序号</th>
								<th >车牌号</th>
								<th >经手人</th>
								<th >维修厂</th>
								<th >维修时间</th>
								<th >维修费用</th>
								<th >项目及费用明细</th>
								<th >公里数</th>
							</tr>
	</script>
	<script type="template" id="template">
		   <tr>
								<td style="width:50px;text-align: center">indexfn</th>
								<td style="width:100px;text-align: center">repairCarNum</th>
								<td style="width:100px;text-align: center">repairHandlePerson</th>
								<td style="width:100px;text-align: center">repairFactory</th>
								<td style="width:100px;text-align: center">repairTime</th>
								<td style="width:100px;text-align: center">repairCost</th>
								<td style="width:200px;text-align: center">repairItemDetail</th>
								<td style="width:100px;text-align: center">repairMileage</th>
			</tr>
	</script>
	<script type="text/javascript">

		var myGrid;
		MTools.autoFullSelect();
		$(".select2").select2();
		$(function() {
			myGrid = MGrid.initGrid({
				url : "${path}/do/admin/bizRepairCost/selectRepairCount",
				onLoadSuccess:function(data){
					if(data.rows){
						$('#printTable').html($('#initTr').html())
						initPrintData(data.rows)
					}
					
				},
			});
			
		});

		/** 获取时间 */
		 function getTimeDD(value, row, index) {
			var temp = "";
			try {
				temp = value
				var date = new Date(temp);
				if (!temp) {
					return "";
				} else {
					return date.getFullYear() + "-" + setFomartZero((date.getMonth() + 1))
					+ "-" + setFomartZero(date.getDate()) ;
				}
			} catch (e) {
				console.err('MGrid getTime执行失败');
			}
			return temp;
		}
		
		 //初始化打印数据
		 function initPrintData(data){
				for(i in data){
					var html=$('#template').html()
					.replace("indexfn",(parseInt(i)+1))
					 .replace("repairCarNum",data[i].repairCarNum)
					 .replace("repairHandlePerson",data[i].repairHandlePerson)
					 .replace("repairFactory",data[i].repairFactory)
					 .replace("repairTime",getTimeDD(data[i].repairTime))
					 .replace("repairCost",data[i].repairCost)
					 .replace("repairItemDetail",data[i].repairItemDetail)
					 .replace("repairMileage",data[i].repairMileage)
					  $("#printTable").append(html);
				}
			}
		 //定义打印对象
		 var LODOP;
			function print() {
			    CreateOneFormPage();
			    LODOP.PRINTA();
			}
			//打印配置
			function CreateOneFormPage() {
			    LODOP = getLodop();
			    LODOP.SET_LICENSES("湖***", "4DCD4C250*********C0B04F2","", "");//打印控件的key
			    LODOP.SET_PRINT_STYLE("FontSize", 18);//设置字体大小
			    //LODOP.SET_PRINT_PAGESIZE(0, 1300, 1945, "");//纸张大小：单位为微米 1300：宽 1945位高
			    LODOP.SET_PRINT_PAGESIZE(2, 0, 0, "A4");
			    LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED",1);//横向时的正向显示
			    strCenterStyle = "<style>table {width:100%}</style>";
			    LODOP.ADD_PRINT_HTM(25, "5%", "95%", "95%", strCenterStyle
			            + document.getElementById("print").innerHTML); //内容居中左右边距5%,上边距25pt
			   // LODOP.SET_PRINT_STYLE("Bold", 1);
			    /*  LODOP.ADD_PRINT_HTM(30,30,650,600,document.getElementById("print").innerHTML); */
			    LODOP.PREVIEW();	
			};
	</script>
</body>
</html>
