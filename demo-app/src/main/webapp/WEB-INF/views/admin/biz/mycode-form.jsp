<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="../images/favicon.ico">
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
<script type="text/javascript"
	src="${path }/resource/plugin/bootstrap-3.3.5/js/bootstrapSwitch.js"></script>
<link rel="stylesheet"
	href="${path }/resource/plugin/bootstrap-3.3.5/css/bootstrapSwitch.css">
<script type="text/javascript"
	src="${path }/resource/js/function/public.js"></script>
<style type="text/css">
table tr td {
	border: none !important;
}

table tr th {
	border: none !important;
}

.inline {
	display: INLINE;
	width: 50%;
}
</style>
</head>
<body>
	<div class="ibox-content">
		<form class="form-horizontal " id="dataform"
			onsubmit="javascripr:return false;">
			<input type="hidden" name="tokenUrl" value="${tokenUrl}"> <input
				type="hidden" name="token" value="${token}">
			<c:if test="${obj ne null }">
				<input type="hidden" name="id" value="${obj.id }">
			</c:if>
			<div class="form-group">

				<label class="col-sm-2 control-label">名称<span
					class="text-danger">*</span></label>
				<div class="col-sm-3">
					<input name="name" type="text" class="form-control "  value="${obj.name }"
						nullmsg="名称不能为空" datatype="*1-50">
					<div class="Validform_checktip"></div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">限制次数<span
					class="text-danger">*</span></label>
				<div class="col-sm-3">
					<input name="codeLimit"   value="${obj.codeLimit }" type="text" class="form-control">
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label">动态二维</label>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered table-bordered  ">
								<thead>
									<tr>
										<th>序号</th>
										<th>扫描次数</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:set var="begin" value="0"></c:set>
									<c:if test="${not empty obj }">
										<c:set var="begin" value="${fn:length(obj.childCodes) }"></c:set>
										<c:forEach items="${obj.childCodes }" var="item"
											varStatus="count">
											<tr>
												<td>${count.index+1}</td>
												<td>${item.total}</td>
												<td><input value="${item.qrcode }"
													name="childCodes[${count.index}].qrcode"
													id="id${count.index}" class="form-control  upload-input"
													type="text" /> <a class="btn btn-primary radius upload-a">
														<i class="fa fa-cloud-upload"></i> 上传二维码
												</a></td>
											</tr>
										</c:forEach>
									</c:if>
									<c:forEach var="item" varStatus="count" begin="${begin }"
										end="5">
										<tr>
											<td>${count.index+1}</td>
											<td>0</td>
											<td><input name="childCodes[${count.index}].qrcode"
												id="id${count.index}" class="form-control  upload-input"
												type="text" /> <a class="btn btn-primary radius upload-a">
													<i class="fa fa-cloud-upload"></i> 上传二维码
											</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>



			<div class="form-group ">
				<div class="col-12 text-center">
					<a href="javascript:;" onclick="myForm.submit()"
						class="btn btn-success radius"><i class="fa fa-check"></i> 保存</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:;" onclick="MTools.closeForm()"
						class="btn btn-danger radius"><i class="fa fa-close"></i> 关闭</a>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
		var invokeUrl = "${path}/do/admin/mycode/addMycode";
		<c:if test="${obj ne null }">
		invokeUrl = "${path}/do/admin/mycode/modifyMycode";
		</c:if>

		var myForm = null;
		$(function() {
			myForm = MForm.initForm({
				invokeUrl : invokeUrl,
				afterSubmit : function() {
					parent.myGrid.serchData();
				}
			});
			MUI.initImgUpload(".upload-input");
		});
	</script>
</body>
</html>