<%--   jsp的基本标签引入，和访问路径变量path，以及自定义标签matrix的引入 --%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<%--   End --%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>
<%--   End --%>

</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<!-- 搜索框部分start -->
		<matrix:btn value="user:search">
			<div class="row">
				<div class="col-sm-12">
					<form class="form-inline" id="serchform">
						<div class="input-group">
							<div class="form-group mr-20">
								<input id="search-text" name="name" placeholder="输入查询关键词"
									type="text" class="form-control">
							</div>
							<div class="form-group mr-20">
								<button onclick="myGrid.serchData(1)" type="button"
									class="btn btn-info">
									<i class="fa fa-search "></i> 搜索
								</button>
								<button type="reset" class="btn btn-info ">
									<i class="fa fa-refresh "></i> 重置
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</matrix:btn>
		<!-- 搜索框部分en -->

		<div class="ibox-content radius-5 mt-5 mpanel">

			<div class="row">
				<div class="col-sm-12">
					<!-- 功能按钮部分 -->
					<div class="option-bar">
						<matrix:btn value="user:dels">
							<button onclick="myGrid.delItems('id')" type="button"
								class="btn btn-danger btn-sm">
								<i class="fa fa-trash"></i>批量删除
							</button>
						</matrix:btn>
						<matrix:btn value="user:add">
							<button onclick="openAdd()" type="button"
								class="btn btn-success btn-sm">
								<i class="fa fa-plus"></i> 新增
							</button>
						</matrix:btn>
					</div>
					<!-- 功能按钮部分end -->

					<!-- 数据表格部分 -->
					<table id="mgrid">
						<thead>
							<tr>
								<th data-checkbox="true"></th>
								<th data-formatter="MGrid.indexfn" data-align="center"
									data-width="30px">序号</th>
								<th data-field="name" data-sortable="true">名称</th>
								<th data-field="codeLimit">活码次数</th>
								<th data-field="qrcode" data-formatter="MGrid.getImage">二维码</th>
								<th data-field="recateTime" data-formatter="MGrid.getTime"
									data-sortable="true">创建时间</th>
								<th data-align="center" data-width="150px" data-field="id"
									data-formatter="buidOperate">操作</th>
							</tr>
						</thead>
					</table>
					<!-- 数据表格部分end -->
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
		//定义表格对象
		var myGrid;

		$(function() {

			//定义删除数据的路径
			var delPath = "";
			//使用权限标签控制路径是否显示在界面上
			delPath = "${path}/do/admin/mycode/del";
			//初始化表格对象
			myGrid = MGrid.initGrid({
				url : "${path}/do/admin/mycode/showList",//url数据加载地址，表格初始化后会自动去后台加载数据。
				delUrl : delPath
			});

		});
		//构建操作栏的按钮
		function buidOperate(value, row, index) {
			var html = [];
			html[0] = '<a onClick="openEdit(\'' + value
					+ '\')"  title="编辑" class="fa fa-edit option"></a>'

			html[1] = '<a  href="'
					+ row.qrcode
					+ '"  target="blank"  )"  title="下载二维码" class="fa fa-cloud-download option"></a>'

			html[2] = '<a onClick="myGrid.delItem(\''
					+ value
					+ '\')" title="删除" class="fa fa-close option text-danger"></a>';

			return html.join("");
		}
		//打开添加界面
		function openAdd() {
			layer.full(layer.open({
				type : 2,
				title : "添加活码",
				maxmin : true,
				area : [ MUI.SIZE_L, '500px' ],
				content : [ '${path}/do/admin/mycode/editForm' ]
			}));
		}
		//打开编辑界面
		function openEdit(id) {
			layer.full(layer.open({
				type : 2,
				title : "编辑活码",
				area : [ MUI.SIZE_L, '500px' ],
				maxmin : true,
				content : [ '${path}/do/admin/mycode/editForm?id=' + id ]
			}));
		}
	</script>
</body>
</html>
