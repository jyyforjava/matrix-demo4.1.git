﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.zkingsoft.com" prefix="matrix"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="../images/favicon.ico">
<!-- 本框架基本脚本和样式 -->
<script type="text/javascript"
	src="${path }/resource/js/plugin/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${path }/resource/js/systools/MBase.js"></script>

<script type="text/javascript" charset="utf-8"
	src="${path }/resource/plugin/beditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"
	src="${path }/resource/plugin/beditor/ueditor.all.js">
	
</script>
<script type="text/javascript" charset="utf-8"
	src="${path }/resource/plugin/beditor/lang/zh-cn/zh-cn.js"></script>

</head>

<div class="ibox-content">
	<form class="form-horizontal" id="dataform"
		onsubmit="javascripr:return false;">
		
		<div class="form-group">
			<label class="col-sm-2 control-label">手机号<span class="text-danger">*</span></label>
			<div class="col-sm-3">
				<input type="text" class="form-control" name="tel" maxlength="11" 
					datatype="*11-11" nullmsg="手机号不能为空">
			</div>
			<a href="javascript:;" onclick="myForm.submit()"
					class="btn btn-success radius">发送</a>
		</div>
			
	</form>

</div>

</body>
	<script type="text/javascript"
	src="${path }/resource/js/systools/MJsBase.js"></script>
	<script type="text/javascript">
	var invokeUrl = "${path}/do/admin/sendSms/send";
	var myForm=MForm.initForm({
		invokeUrl:invokeUrl
	});
	 /* $.AjaxProxy({
			p : {
				ids:ids,
				checkStatus:3,
				reason:null
			}
		}).invoke("${path}/do/admin/productCheck/su/firstTrial", function(loj) {
			 layer.close(index);
			if(loj.getValue("status")=='200'){
				myGrid.serchData();
				layer.msg(loj.getValue("info"),{icon:1,time:2000})
			}else{
				layer.msg(loj.getValue("info"),{icon:2,time:2000})
			}
		});  */
	</script>
</html>