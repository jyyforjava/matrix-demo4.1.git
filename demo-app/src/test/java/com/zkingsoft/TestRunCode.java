package com.zkingsoft;

import java.io.File;
import java.io.FileReader;

import org.junit.Test;

import com.zkingsoft.common.groovy.GlueFactory;

public class TestRunCode {

	@Test
	public void test() throws Exception {
		FileReader in=new FileReader(new File("/Users/jiangyouyao/java/projects/matrix-demo4.1/demo-app/src/main/java/com/zkingsoft/common/groovy/DefaultGroovyRunable.java"));
		char[] chars=new char[1024];
		int i=0;
		StringBuilder s=new StringBuilder();
		while((i=in.read(chars))>0){
			s.append(new String(chars,0,i));
		}
		
		String code=s.toString();
		try {
			Object a= GlueFactory.getInstance().loadNewInstance(code);
			System.out.println(a);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
